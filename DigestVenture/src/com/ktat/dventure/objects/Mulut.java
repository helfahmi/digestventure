package com.ktat.dventure.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;

public class Mulut extends Image{
	private Array<AtlasRegion> images;
	
	public Mulut(Array<AtlasRegion> frames){
		super(frames.get(0));
		images = frames;
	}
	
	@Override
	public void act(float delta){
	}
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha){
		if(Gdx.input.isTouched()){
        	int x = Gdx.input.getX();
            int y = Gdx.input.getY();
            
            Vector3 v = new Vector3(x, y, 0);
            getStage().getCamera().unproject(v);
            Gdx.app.log("Mouse", "x,y: " +v.x + " " + v.y);
            
            if(v.x >= 130 && v.x <= 342 && v.y >= 74 && v.y <= 180)
            	batch.draw(images.get(1), 0, 0);
            else
            	batch.draw(images.get(0), 0, 0);
		} else {
			batch.draw(images.get(0), 0, 0);
		}
	}
}
