package com.ktat.dventure.objects;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.Element;

public class FunFact {
	class Fact{
		private String name, fact1, fact2;
		
		public Fact(String name, String fact1, String fact2){
			this.name = name;
			this.fact1 = fact1;
			this.fact2 = fact2;
		}
		
		public String getFact(){
			Random random = new Random();
			if (random.nextInt(2)==0){
				return fact1;
			} else {
				return fact2;
			}
		}
	}
	
	private ArrayList<Fact> facts;
	
	public FunFact() throws IOException{
		facts = new ArrayList<FunFact.Fact>();
		XmlReader reader = new XmlReader();
		Element root = reader.parse(Gdx.files.internal("data/funfact.xml"));
		Array<Element> items = root.getChildrenByName("facts");
		for (Element child : items)
		{
			String name = child.getChildByName("name").getAttribute("text");
		    String info1 = child.getChildByName("fact1").getAttribute("text");
		    String info2 = child.getChildByName("fact2").getAttribute("text");
		    Fact fact = new Fact(name,info1,info2);
		    facts.add(fact);
		}
	}
	
	public String getFact(int index){
		return facts.get(index).getFact();
	}
	
	
}
