package com.ktat.dventure.objects;

import com.badlogic.gdx.tools.imagepacker.TexturePacker2;
import com.badlogic.gdx.tools.imagepacker.TexturePacker2.Settings;

/* Run file ini untuk ngepack resource yang ada di assets. 
 * ------------------------------------------
 * Parameter 1: Direktori asal.
 * Parameter 2: Direktori tujuan.
 * Parameter 3: Nama atlas yang terbentuk, tolong jangan diganti.
 * teeeest sdasdas
 */
public class MyPacker {
        public static void main (String[] args) throws Exception {
        		Settings settings = new Settings();
            	settings.maxWidth = 2048;
            	settings.maxHeight = 2048;
                TexturePacker2.process(settings, "../DigestVenture-android/assets/splash", "../DigestVenture-android/assets/atlas", "DVsplash");
                TexturePacker2.process(settings, "../DigestVenture-android/assets/images", "../DigestVenture-android/assets/atlas", "DVmain");
        }
}