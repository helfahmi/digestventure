package com.ktat.dventure.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.ktat.dventure.main.DVenture;
import com.ktat.dventure.screen.TongueScreen;

public class ClickableFood extends Actor{
	
	private TongueScreen parent;
	private TextureRegion food, fork;
	private boolean isForkVisible;
	public ClickableFood(TongueScreen parent, TextureRegion food, TextureRegion fork){
		this.parent = parent;
		this.food = food;
		this.fork = fork;
		isForkVisible = false;
		setBounds(0, 0, food.getRegionWidth(), food.getRegionHeight());
		addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				Gdx.app.log(DVenture.LOG, "clicked!");
				ClickableFood actor = ((ClickableFood) event.getTarget());
				if(actor.isDown())
					actor.release();
				else
					actor.setDown();
			}
		});
	}
	
	public boolean isDown(){
		return isForkVisible;
	}
	
	public void release(){
		isForkVisible = false;
		parent.setClickedFood("none");
	}
	
	public void setDown(){
		parent.removeForks();
		isForkVisible = true;
		parent.setClickedFood(getName());
	}
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		batch.draw(food, getX(), getY());
		if(isForkVisible)
			batch.draw(fork, getX() - 30, getY() + 10);
	}
}
