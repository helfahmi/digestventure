package com.ktat.dventure.objects;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.ktat.dventure.main.DVenture;

public class DialogManager extends Actor{
//test	
	public enum Direction{
		LEFT, RIGHT, NONE
	};
	private class MsgFormat{
		private String message;
		
		public String getMsg() {
			return message;
		}
		
		public float x,y,arrx,arry;
		public Direction dir;
		public Runnable r;
		
		public Direction getDirection(){
			return dir;
		}
		
		public MsgFormat(String msg, float sx, float sy, float ax, float ay, Direction d){
			message = msg;
			x = sx;
			dir = d;
			arrx = ax;
			arry = ay;
			y = sy;
			r = null;
		}
		
		public MsgFormat(String msg, float sx, float sy){
			message = msg;
			x = sx;
			y = sy;
			dir = Direction.NONE;
			arrx = 0;
			arry = 0;
			r = null;
		}
		
		public MsgFormat(String msg, float sx, float sy, Runnable r){
			message = msg;
			x = sx;
			y = sy;
			dir = Direction.NONE;
			arrx = 0;
			arry = 0;
			this.r = r;
		}
	}
	
	private ArrayList<MsgFormat> messages;
	private int curMsg;
	private Skin skin;
	private AtlasRegion msgBg;
	private Image leftPointer, rightPointer;
	private Label curLabel, skip;
	private float leftMargin, bottomMargin;
	private Runnable onFinishAction;
	private boolean isSkipEnabled = false;
	
	private Direction curDir;

	public DialogManager(AtlasRegion atlasRegion, AtlasRegion left, AtlasRegion right){
		super();
		onFinishAction = null;
		messages = new ArrayList<MsgFormat>();
		curMsg = 0;
		leftMargin = 15f;
		bottomMargin = 20f;
		setVisible(false);
		
		skin = new Skin(Gdx.files.internal("skins/menuskin.json"));
		leftPointer = new Image(left);
		rightPointer = new Image(right);
		curDir = Direction.NONE;
		leftPointer.addAction(Actions.forever(Actions.sequence(Actions.moveBy(-20, 0, 1f, Interpolation.exp10Out), Actions.moveBy(20, 0, 1f, Interpolation.exp10Out))));
		rightPointer.addAction(Actions.forever(Actions.sequence(Actions.moveBy(-20, 0, 1f, Interpolation.exp10Out), Actions.moveBy(20, 0, 1f, Interpolation.exp10Out))));		
		this.msgBg = atlasRegion;
		this.setBounds(0, 0, msgBg.packedWidth, msgBg.packedHeight);
		addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				next();
				doMsgAction();
			}
		});
	}
	
	public void setSkipEnabled(boolean b){
		isSkipEnabled = b;
	}
	
	public void reset(){
		messages.clear();
		curMsg = 0;
		onFinishAction = null;
		setVisible(false);
	}
	
	public void init(){
		setPosition(messages.get(curMsg).x, messages.get(curMsg).y);
		curLabel = new Label((CharSequence) messages.get(curMsg).getMsg(), skin);
		curLabel.setPosition(messages.get(curMsg).x + leftMargin, messages.get(curMsg).y + bottomMargin);
		curLabel.setFontScale(0.3f);
		
		this.setColor(getColor().r, getColor().g, getColor().b, 0);
		curLabel.setColor(curLabel.getColor().r, curLabel.getColor().g, curLabel.getColor().b, 0);
		addAction(Actions.fadeIn(0.5f));
		curLabel.addAction(Actions.fadeIn(0.5f));
		
		curDir = messages.get(curMsg).getDirection();
		if(curDir == Direction.LEFT)
			leftPointer.setPosition(messages.get(curMsg).arrx, messages.get(curMsg).arry);
		else if(curDir == Direction.RIGHT)
			rightPointer.setPosition(messages.get(curMsg).arrx, messages.get(curMsg).arry);
		setVisible(true);
	}
	
	public void setOnFinishAction(Runnable onFinishAction) {
		this.onFinishAction = onFinishAction;
	}
	
	public void addMsg(String msg, int x, int y){
		messages.add(new MsgFormat(msg, x, y));
	}
	
	public void addMsg(String msg, int x, int y, int arrx, int arry, Direction dir){
		messages.add(new MsgFormat(msg, x, y, arrx, arry, dir));
	}
	
	public void addMsg(String msg, int x, int y, Runnable run){
		messages.add(new MsgFormat(msg, x, y, run));
	}
	
	Runnable onDialogFinish = new Runnable(){
		@Override
		public void run() {
			reset();
			remove();
		}
	};
	
	public void doMsgAction(){
		if(curMsg < messages.size()){
			if(messages.get(curMsg).r != null){
				addAction(Actions.run(messages.get(curMsg).r));
			}
		}
	}
	
	public void next(){
		Gdx.app.log(DVenture.LOG, "message ke-" + curMsg + " udah ditulis.");
		curMsg++;
		
		if(curMsg == messages.size()){
			if(onFinishAction == null)
				addAction(Actions.sequence(Actions.fadeOut(0.5f), Actions.run(onDialogFinish)));
			else
				addAction(Actions.sequence(Actions.fadeOut(0.5f), Actions.run(onFinishAction), Actions.run(onDialogFinish)));
			curLabel.addAction(Actions.fadeOut(0.5f));
			
		} else {
			setTouchable(Touchable.disabled);
			addAction(Actions.sequence(Actions.fadeOut(0.5f), Actions.run(setNewMsg), Actions.fadeIn(0.5f), Actions.run(enableTouch)));
			curLabel.addAction(Actions.sequence(Actions.fadeOut(0.5f), Actions.fadeIn(0.5f)));
//			setPosition(messages.get(curMsg).x, messages.get(curMsg).y);
//			curLabel.setText((CharSequence) messages.get(curMsg).getMsg());
//			curLabel.setPosition(messages.get(curMsg).x, messages.get(curMsg).y);
		}
	}
Runnable enableTouch = new Runnable(){

	@Override
	public void run() {
		setTouchable(Touchable.enabled);
	}
	
};

Runnable setNewMsg = new Runnable(){
	@Override
	public void run() {
		setNewMsg();
	}	
};
	public void setNewMsg(){
		setPosition(messages.get(curMsg).x, messages.get(curMsg).y);
		curLabel.setText((CharSequence) messages.get(curMsg).getMsg());
		
		curDir = messages.get(curMsg).getDirection();
		
		if(curDir == Direction.LEFT)
			leftPointer.setPosition(messages.get(curMsg).arrx, messages.get(curMsg).arry);
		else if(curDir == Direction.RIGHT)
			rightPointer.setPosition(messages.get(curMsg).arrx, messages.get(curMsg).arry);
		
		float decreaseMargin = 0f;
		if(messages.get(curMsg).getMsg().contains("\n")){
			decreaseMargin += 10f;
		}
		
		curLabel.setPosition(messages.get(curMsg).x + leftMargin, messages.get(curMsg).y + bottomMargin - decreaseMargin);
	}
	
	
	
	@Override
	public void act(float delta){
		super.act(delta);
		curLabel.act(delta);
		if(curDir == Direction.LEFT)
			leftPointer.act(delta);
		else if(curDir == Direction.RIGHT)
			rightPointer.act(delta);
	}
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha){
		super.draw(batch, parentAlpha);
		if(curDir == Direction.LEFT)
			leftPointer.draw(batch, parentAlpha);
		else if(curDir == Direction.RIGHT)
			rightPointer.draw(batch, parentAlpha);
		batch.setColor(this.getColor());
		batch.draw(msgBg, getX(), getY());
		curLabel.draw(batch, parentAlpha);

	}
}
