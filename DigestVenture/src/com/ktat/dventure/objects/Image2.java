package com.ktat.dventure.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.ktat.dventure.main.DVenture;

public class Image2 extends Actor{
	
	private Texture texture;
	public Image2(Texture text){
		super();
		texture = text;
		//texture = new TextureRegion(region);
		if(texture == null)
			Gdx.app.log(DVenture.LOG, "texture is null.");
	}
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha){
		Gdx.app.log(DVenture.LOG, "Image2 drawn.");
		batch.draw(texture, getX(), getY(), getWidth(), getHeight());
	}
}
