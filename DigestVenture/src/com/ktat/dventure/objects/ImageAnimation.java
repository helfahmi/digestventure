package com.ktat.dventure.objects;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;

public class ImageAnimation extends Image{
	private Animation anim;
	private float animStateTime;
	private TextureRegion curFrame;
	public ImageAnimation(Array<AtlasRegion> img, float animSpeed){
		super(img.get(0));
		
		this.anim = new Animation(animSpeed, img);
		animStateTime = 0;
	}
	
	public ImageAnimation(Array<AtlasRegion> img, float animSpeed, int playType){
		super(img.get(0));
		
		this.anim = new Animation(animSpeed, img, playType);
		animStateTime = 0;
	}
	
	public Animation getAnimation() {
		return anim;
	}
	@Override
	public void act(float delta){
		super.act(delta);
		this.setColor(getColor());
		
		animStateTime += delta;                     
        curFrame = anim.getKeyFrame(animStateTime, true);  
	}
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha){
		batch.setColor(this.getColor());
		batch.draw(curFrame, getX(), getY(), curFrame.getRegionWidth()*getScaleX(), curFrame.getRegionHeight()*getScaleY());
	}
}
