package com.ktat.dventure.objects;

import java.util.Random;

import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.ktat.dventure.screen.StomachScreen;

public class Bubble extends ImageButton{
	private String type;
	private int speed;
	
	public Bubble(String type, StomachScreen screen){
		super(new TextureRegionDrawable(screen.getAtlas().findRegion(type)));
		this.type = type;
		speed = new Random().nextInt(50) + 50;
	}
	
	public String getType(){
		return type;
	}
	
	public int getSpeed(){
		return speed;
	}
	
	public void setType(String _type){
		type = _type;
	}
	
	public void setSpeed(int speed){
		this.speed = speed;
	}
	
	public void randomSpeed(){
		speed = new Random().nextInt(50) + 50; 
	}
	
}
