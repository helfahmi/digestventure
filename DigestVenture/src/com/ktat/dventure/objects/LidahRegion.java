package com.ktat.dventure.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.ktat.dventure.main.DVenture;

public class LidahRegion extends Actor{
	
	private AtlasRegion up, down, used;
	public LidahRegion(final AtlasRegion up, final AtlasRegion down){
		this.up = up;
		this.down = down;
		used = up;
		setBounds(0, 0, up.packedWidth, up.packedHeight);
		addListener(new InputListener(){
			@Override
			public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
				used = down;
				Gdx.app.log(DVenture.LOG, "mouse entered some lidah region");
			}
			
			@Override
			public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
				used = up;
				Gdx.app.log(DVenture.LOG, "mouse exited some lidah region");
			}
		});
	}
	
	@Override
	public void act(float delta) {
		super.act(delta);
	}
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		batch.draw(used, getX(), getY());
	}
	
	
}
