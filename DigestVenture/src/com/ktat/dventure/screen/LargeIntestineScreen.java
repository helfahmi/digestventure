package com.ktat.dventure.screen;


import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.ktat.dventure.main.DVenture;
import com.ktat.dventure.objects.DialogManager;
import com.ktat.dventure.objects.ImageAnimation;

public class LargeIntestineScreen extends AbstractScreen{
	private Image sidebarR, sidebarL, bg;
	private ImageAnimation brain;
	private float counterBar;
	
	private float speed = 3;
	private float progress = 0;
	private float time = 0;
	private String status = "tutorial";

	//Stage spesific
	private Image intestine, faeces;
	private int points; 
	private ImageButton mainButton;
	private int counter;
	private Image gauge, gaugeBar;
	private Image mulai;
	private DialogManager dialog;
	
	
	public LargeIntestineScreen(DVenture game) {
		super(game);
		//Images
		sidebarR = new Image(getAtlas().findRegion("sidebar"));
		sidebarL = new Image(getAtlas().findRegion("sidebar"));
		bg = new Image(getAtlas().findRegion("bg_game"));
		bg.setX(150);
		brain = new ImageAnimation(getAtlas().findRegions("brainleft"), 0.050f);
		intestine = new Image(getAtlas().findRegion("bg_colon"));
		faeces = new Image(getAtlas().findRegion("shit"));
		mainButton = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("tap_unpressed")), new TextureRegionDrawable(getAtlas().findRegion("tap_pressed")));
		gaugeBar = new Image(getAtlas().findRegion("gaugebar"));
		mulai = new Image(getAtlas().findRegion("go"));		
		mulai.setPosition((800 - mulai.getWidth())/2, (480 - mulai.getHeight())/2);
		mulai.addAction(Actions.alpha(0f));
		counter = 0;
		points = 0;

		gauge = new Image(getAtlas().findRegion("gauge"));
		
		int intestineX = 220;
		int intestineY = 0;
		
		intestine.setX(intestineX);
		intestine.setY(intestineY);
		
		faeces.setPosition(intestineX+100, 480);
		
		sidebarR.setX(800 - sidebarR.getWidth());
		brain.setPosition(600, 328);
		
		progress = 0;
		counterBar = 0;
		mainButton.setPosition(660, 100);
		
		dialog = new DialogManager(getAtlas().findRegion("dialogbox"), getAtlas().findRegion("pointer_left"),  getAtlas().findRegion("pointer_right"));
		dialog.addMsg("Tekan tombol sesuai dengan bola", 150, 100);
		dialog.setOnFinishAction(new Runnable(){
			@Override
			public void run() {
				mulai.addAction(Actions.sequence(Actions.fadeIn(0.5f), Actions.delay(0.5f), Actions.run(new Runnable(){
					@Override
					public void run() {
						mulai.remove();
						status = "belom";
					}
				})));
			}
		});
		dialog.init();
		
		gaugeBar.setPosition(intestineX+50, 50);
		gauge.setPosition(intestineX+50, 40);
		
		//Staging actors
		stage.addActor(bg);
		stage.addActor(sidebarR);
		stage.addActor(sidebarL);
		stage.addActor(brain);
		stage.addActor(mainButton);
		stage.addActor(intestine);
		stage.addActor(faeces);
		stage.addActor(gaugeBar);
		stage.addActor(gauge);
		stage.addActor(mulai);
		
		loadKepala();
		loadInfus();
		hany.setScaleY(progress/100f);
		poison.setScaleY(time/100f);
		loadHelpPauseButton();
		stage.addActor(dialog);
		loadPauseScreen();
		
		//tutorial
		dialog = new DialogManager(getAtlas().findRegion("dialogbox"), getAtlas().findRegion("pointer_left"),  getAtlas().findRegion("pointer_right"));
		dialog.addMsg("Tap tombol ini", 150, 250);
		dialog.addMsg("Jaga pada kisaran ini", 150, 200);
		dialog.setOnFinishAction(new Runnable(){
			@Override
			public void run() {
				status = "belom";
			}
		});
		dialog.init();
		
		stage.addActor(dialog);
	}
	
	@Override
	public void render(float delta){
		super.render(delta);
		super.updatePause();
		if (status == "menang") {
			System.gc();
			int score;
			int poisons = (int) (poison.getScaleY()*100f);
			score = poisons * 50;
			game.setArcadeScore(7, score);
			startKepalaAnimation(status, new ScoreScreen(game, "LargeIntestineScreen"));
		} else if (status == "kalah") {
			System.gc();
			startKepalaAnimation(status, new GameOverScreen(game));
		}
		if (!getPause() && status == "belom") {		
			int regression = new Random().nextInt(10);
			if (mainButton.isPressed()){
				int addition = new Random().nextInt(10)+10;
				counter += addition;
			}
			counter -= regression;
			if (counter > 500){
				counter = 500;
			} else if (counter <= 0){
				counter = 0;
			}
			if ((counter > 140)&&(counter < 210)){
				faeces.setY(faeces.getY()-80*delta);
				if (faeces.getY() >= 480)
					gauge.setVisible(false);
			}
			
			gauge.setX(counter+270);	
			if (gauge.getX()>510){
				gauge.setX(510);
			}
			
			//progress bar smoothing
			if (counterBar < progress){
				counterBar += 0.5f;
			} else if (counterBar > progress){
				counterBar -= 1;
			}
			counterBar = 100 - faeces.getY()/4.8f;
			if (counterBar > 100){
				counterBar = 100;
			}
			
			if (status == "belom"){
				hany.setScaleY(counterBar/100f);
			}
			//progress = (int)counterBar;
			
			//Infus
			if (progress >= 100) {
				status = "menang";
			} if (poison.getScaleY() >= 1) {
				status = "kalah";
			}
			if (poison.getScaleY() < 1) {
				time += speed;
				poison.setScaleY((float) (time*0.05/100f));
			}
		}
	}
}
