package com.ktat.dventure.screen;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.ktat.dventure.main.DVenture;
import com.ktat.dventure.objects.ImageAnimation;

public class FinishedScreenLight extends AbstractScreen {
	private Image bgLight;
	private ImageAnimation organ[];
	private ImageButton next;
	
	public FinishedScreenLight(final DVenture game) {
		super(game);
		bgLight = new Image(getAtlas().findRegion("bg_light"));
		//organs
		organ = new ImageAnimation[8];
		organ[0] = new ImageAnimation(getAtlas().findRegions("brainleft"), 0.90f);
		organ[1] = new ImageAnimation(getAtlas().findRegions("tongueleft"), 0.90f, Animation.LOOP_PINGPONG);
		organ[2] = new ImageAnimation(getAtlas().findRegions("tongueright"), 0.90f, Animation.LOOP_PINGPONG);
		organ[3] = new ImageAnimation(getAtlas().findRegions("esophagusleft"), 0.90f);
		organ[4] = new ImageAnimation(getAtlas().findRegions("stomachright"), 0.90f);
		organ[5] = new ImageAnimation(getAtlas().findRegions("duodenumright"), 0.90f);
		organ[6] = new ImageAnimation(getAtlas().findRegions("intestine"), 0.90f, Animation.LOOP_PINGPONG);
		organ[7] = new ImageAnimation(getAtlas().findRegions("colon"), 0.90f);
		organ[0].setPosition(610, 350);
		organ[1].setPosition(320, 20);
		organ[2].setPosition(270, 30);
		organ[3].setPosition(650, 20);
		organ[4].setPosition(350, 30);
		organ[5].setPosition(50, 20);
		organ[6].setPosition(530, 50);
		organ[7].setPosition(30, 70);
		next = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("play_untouched")), new TextureRegionDrawable(getAtlas().findRegion("play_touched")));
		next.setPosition(800 - next.getWidth() - 15, 15);
		next.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				fadeOutAndChangeTo(new MenuScreen(game));
			}
		});
		stage.addActor(bgLight);
		stage.addActor(next);
		for (int i=0;i<8;i++){
			stage.addActor(organ[i]);
		}
	}

}
