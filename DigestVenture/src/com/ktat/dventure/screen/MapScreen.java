package com.ktat.dventure.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.ktat.dventure.main.DVenture;
import com.ktat.dventure.objects.ImageAnimation;

public class MapScreen extends AbstractScreen{

	private Image bg, path, lvlPlayed, lvlPlayable, lvlUnplayable;
	private ImageButton lvlBrain, lvlTongue, lvlEsophagus, lvlIntestine, lvlDuodenum, lvlStomach, lvlKosong, lvlColon;
	private ImageAnimation brainR, tongueL, tongueR, esophagusL, intestine, duodenumR, stomachR, colon;
	private Image shadow[] = new Image[8];
	private ImageButton levelButton[] = new ImageButton[8];
	private ImageButton back;
	private Label scoreLabel[] = new Label[8];
	//temporary//
	/* 	
	 * Edit kalo udah bisa akses data
	 */
	private int levelScore[] = new int[8];
	private int curLevel;
	
	public MapScreen(final DVenture game) {
		super(game);
		//dummy
		curLevel = game.getLastLevelCompleted();
		
		for (int i=0;i<8;i++){
			levelScore[i] = game.getArcadeScore(i);
		}				
		
		bg = new Image(getAtlas().findRegion("bg_level"));
		path = new Image(getAtlas().findRegion("level_path"));
		lvlPlayable = new Image(getAtlas().findRegion("level_playable"));
		lvlPlayed = new Image(getAtlas().findRegion("level_played"));
		lvlUnplayable = new Image(getAtlas().findRegion("level_unplayable"));
		
		/**
		lvlBrain = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("level_playable")));
		lvlTongue = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("level_unplayable")));
		lvlEsophagus = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("level_unplayable")));
		lvlIntestine = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("level_unplayable")));
		lvlDuodenum = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("level_unplayable")));
		lvlStomach = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("level_unplayable")));
		lvlKosong = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("level_unplayable")));
		lvlColon = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("level_unplayable")));
		 * 
		 */
		back = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("back_untouched")), new TextureRegionDrawable(getAtlas().findRegion("back_touched")));
		back.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				fadeOutAndChangeTo(new MenuScreen(game));
			}
		});
		back.setX(15);
		back.setY(480 - back.getHeight() - 15);
		for (int i=0;i<8;i++){
			if (i>curLevel){
				levelButton[i] = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("level_unplayable")));
			} else if (i == curLevel){
				levelButton[i] = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("level_playable")));
			} else if (i<curLevel){
				levelButton[i] = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("level_played")));
			}
		}
		
		
		/* =========================
		 * Masih nguli, kalo bisa edit
		 * ========================= 
		 */
		//fgyfghfghfg
		brainR = new ImageAnimation(getAtlas().findRegions("brainright"), 0.090f);
		if (curLevel>0){
			tongueL = new ImageAnimation(getAtlas().findRegions("tongueleft"), 0.090f);
			tongueR = new ImageAnimation(getAtlas().findRegions("tongueright"), 0.090f);
		} else {
			tongueL = new ImageAnimation(getAtlas().findRegions("tongueleft_silhouette"), 0.090f);
			tongueR = new ImageAnimation(getAtlas().findRegions("tongueright_silhouette"), 0.090f);
		}
		if (curLevel>1){
			esophagusL = new ImageAnimation(getAtlas().findRegions("esophagusleft"), 0.090f);
		} else {
			esophagusL = new ImageAnimation(getAtlas().findRegions("esophagus_silhouette"), 0.090f);
		}
		if (curLevel>3){
			intestine = new ImageAnimation(getAtlas().findRegions("intestine"), 0.090f);
		} else {
			intestine = new ImageAnimation(getAtlas().findRegions("intestine_silhouette"), 0.090f);
		}
		if (curLevel>4){
			duodenumR = new ImageAnimation(getAtlas().findRegions("duodenumright"), 0.090f);
		} else {
			duodenumR = new ImageAnimation(getAtlas().findRegions("duodenum_silhouette"), 0.090f);
		}
		if (curLevel>2){
			stomachR = new ImageAnimation(getAtlas().findRegions("stomachright"), 0.090f);
		} else {
			stomachR = new ImageAnimation(getAtlas().findRegions("stomachright_silhouette"), 0.090f);
		}
		if (curLevel>6){
			colon = new ImageAnimation(getAtlas().findRegions("colon"), 0.090f);
		} else {
			colon = new ImageAnimation(getAtlas().findRegions("colon_silhouette"), 0.090f);
		}

		//set shadow
		for (int i=0;i<8;i++){
			shadow[i] = new Image(getAtlas().findRegion("shadow"));
		}
		
		path.setPosition(109, 59);
		
		brainR.setPosition(98, 377);
		levelButton[0].setPosition(200, 366);
		shadow[0].setPosition(98, 367);
		
		tongueR.setPosition(391, 407);
		tongueL.setPosition(419, 445);
		levelButton[1].setPosition(438, 366);
		shadow[1].setPosition(391, 397);
		shadow[2].setPosition(419, 435);
		
		esophagusL.setPosition(728, 280);
		levelButton[2].setPosition(651, 269);
		shadow[3].setPosition(728, 260);
		
		intestine.setPosition(34, 212);
		levelButton[3].setPosition(549, 159);
		shadow[4].setPosition(34, 192);
		
		duodenumR.setPosition(236, 212);
		levelButton[4].setPosition(295, 159);
		shadow[5].setPosition(236, 192);
		
		stomachR.setPosition(469, 212);
		levelButton[5].setPosition(86, 159);
		shadow[6].setPosition(469, 192);
		
		levelButton[6].setPosition(226, 39);
		
		colon.setPosition(570, 39);
		levelButton[7].setPosition(475, 39);
		shadow[7].setPosition(600, 20);

		//label score
		for (int i=0;i<8;i++){
			String score = Integer.toString(levelScore[i]);
			if (i>=curLevel){
				score = "    ";
			}
			scoreLabel[i] = new Label((score), getSkin());
			scoreLabel[i].setFontScale(0.4f);
			scoreLabel[i].setPosition(levelButton[i].getX()-5, levelButton[i].getY()-20);
		}

		scoreLabel[0].addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.playButtonSound();
				super.clicked(event, x, y);
				if(game.getLevelCompleted(0)){
					game.pauseMenu();
					game.playArcade();
					fadeOutAndChangeTo(new TongueScreen(game));
				}
			}
		});
		
		scoreLabel[1].addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.playButtonSound();
				super.clicked(event, x, y);
				if(game.getLevelCompleted(1)){
					game.pauseMenu();
					game.playArcade();
					fadeOutAndChangeTo(new GigiScreen(game));
				}
			}
		});
		
		scoreLabel[2].addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.playButtonSound();
				super.clicked(event, x, y);
				if(game.getLevelCompleted(2)){
					game.pauseMenu();
					game.playArcade();
					fadeOutAndChangeTo(new AmilaseScreen(game));
				}
			}
		});
		
		scoreLabel[3].addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.playButtonSound();
				super.clicked(event, x, y);
				if(game.getLevelCompleted(3)){
					game.pauseMenu();
					game.playArcade();
					fadeOutAndChangeTo(new EsophagusScreen(game));
				}
			}
		});
		
		scoreLabel[4].addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.playButtonSound();
				super.clicked(event, x, y);
				if(game.getLevelCompleted(4)){
					game.pauseMenu();
					game.playArcade();
					fadeOutAndChangeTo(new StomachScreen(game));
				}
			}
		});
		
		scoreLabel[5].addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.playButtonSound();
				super.clicked(event, x, y);
				if(game.getLevelCompleted(5)){
					game.pauseMenu();
					game.playArcade();
					fadeOutAndChangeTo(new DuabelasScreen(game));
				}
			}
		});
		
		scoreLabel[6].addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.playButtonSound();
				super.clicked(event, x, y);
				if(game.getLevelCompleted(6)){
					game.pauseMenu();
					game.playArcade();
					fadeOutAndChangeTo(new UsushalusScreen(game));
				}
			}
		});
		
		scoreLabel[7].addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.playButtonSound();
				super.clicked(event, x, y);
				if(game.getLevelCompleted(7)){
					game.pauseMenu();
					game.playArcade();
					fadeOutAndChangeTo(new LargeIntestineScreen(game));
				}
			}
		});
		
		
		brainR.setScale(0.5f);
		//shadow[0].setScale(0.5f);
		tongueR.setScale(0.5f);
		//shadow[1].setScale(0.5f);
		tongueL.setScale(0.5f);
		//shadow[2].setScale(0.5f);
		esophagusL.setScale(0.5f);
		//shadow[3].setScale(0.8f);
		intestine.setScale(0.5f);
		//shadow[4].setScale(0.5f);
		duodenumR.setScale(0.5f);
		//shadow[5].setScale(0.5f);
		stomachR.setScale(0.5f);
		//shadow[6].setScale(0.5f);
		colon.setScale(0.5f);
		//shadow[7].setScale(0.8f);
		
		stage.addActor(bg);
		for (int i=0;i<8;i++){
			//JOROK soalnya ada shadow yg ga jadi dpake
			if (i!=2)
				stage.addActor(shadow[i]);
		}
		stage.addActor(path);
		/**
		 * stage.addActor(lvlBrain);
		stage.addActor(lvlTongue);
		stage.addActor(lvlEsophagus);
		stage.addActor(lvlIntestine);
		stage.addActor(lvlDuodenum);
		stage.addActor(lvlStomach);
		stage.addActor(lvlKosong);
		stage.addActor(lvlColon);
		 */
		for (int i=0;i<8;i++){
			stage.addActor(levelButton[i]);
		}
		
		stage.addActor(brainR);
		stage.addActor(tongueL);
		stage.addActor(tongueR);
		stage.addActor(esophagusL);
		stage.addActor(intestine);
		stage.addActor(duodenumR);
		stage.addActor(stomachR);
		stage.addActor(colon);
		stage.addActor(back);
		
		for (int i=0;i<8;i++){
			if (i<=curLevel){
				stage.addActor(scoreLabel[i]);
			}
		}
		
		fadeIn();
	}

}
