package com.ktat.dventure.screen;

import org.w3c.dom.css.CSSPrimitiveValue;
import org.w3c.dom.css.Rect;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.ktat.dventure.main.DVenture;
import com.ktat.dventure.objects.DialogManager;
import com.ktat.dventure.objects.ImageAnimation;
import com.ktat.dventure.objects.DialogManager.Direction;

public class DuabelasScreen extends AbstractScreen{
	private Image bg, duodenum, left, right, center, sidebarR, sidebarL;
	private ImageAnimation brain;
	private Image protein[], lemak[], gula[];
	private ImageButton rLemak, rProtein, rGula;
	
	private float speed = 1f;
	private float progress = 0;
	private float time = 0;
	private String status = "tutorial"; //belom, menang, kalah
	private DialogManager dmanager;
	private final int POINT_SIZE = 20;
	private Image mulai;
	
	public DuabelasScreen(final DVenture game) {
		super(game);
		
		//STATIC IMAGE
		brain = new ImageAnimation(getAtlas().findRegions("brainleft"), 0.050f);
		bg = new Image(getAtlas().findRegion("bg_game"));
		bg.setX(150);
		duodenum = new Image(getAtlas().findRegion("bg_duodenum"));
		left = new Image(getAtlas().findRegion("lineleft"));
		right = new Image(getAtlas().findRegion("lineright"));
		center = new Image(getAtlas().findRegion("linecenter"));
		sidebarR = new Image(getAtlas().findRegion("sidebar"));
		sidebarL = new Image(getAtlas().findRegion("sidebar"));
		mulai = new Image(getAtlas().findRegion("go"));		
		mulai.setPosition((800 - mulai.getWidth())/2, (480 - mulai.getHeight())/2);
		mulai.addAction(Actions.alpha(0f));
		
		//DYNAMIC IMAGE
		protein = new Image[20];
		for (int i = 0; i < 10; i++) {
			protein[i] = new Image(getAtlas().findRegion("protein"));
		}
		
		lemak = new Image[20];
		for (int i = 0; i < 10; i++) {
			lemak[i] = new Image(getAtlas().findRegion("fat"));
		}
		
		gula = new Image[20];
		
		for (int i = 0; i < 10; i++) {
			gula[i] = new Image(getAtlas().findRegion("glucose"));
		}
		
		//BUTTON
		rGula = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("glucose_tap_unpressed")), new TextureRegionDrawable(getAtlas().findRegion("glucose_tap_pressed"))); 
		rProtein = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("protein_tap_unpressed")), new TextureRegionDrawable(getAtlas().findRegion("protein_tap_pressed"))); 
		rLemak = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("fat_tap_unpressed")), new TextureRegionDrawable(getAtlas().findRegion("fat_tap_pressed"))); 
		
		rGula.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Rectangle r = new Rectangle(rGula.getX(), rGula.getY(), rGula.getWidth(), rGula.getHeight());
				if (r.contains(gula[1].getX(), gula[1].getY())) {
					gula[1].setX(267+rGula.getWidth());
					gula[1].setY(480 - gula[1].getHeight() - 115);
					if (status == "belom"){
						progress += POINT_SIZE;
						hany.setScaleY(progress/100f);
					}
				}
			}
		});
		rProtein.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				
				Rectangle r = new Rectangle(rProtein.getX(), rProtein.getY(), rProtein.getWidth(), rProtein.getHeight());
				if (r.contains(protein[1].getX(), protein[1].getY())) {
					protein[1].setX(413);
					protein[1].setY(480 - protein[1].getHeight() - 115);
					if (status == "belom"){
						progress += POINT_SIZE;
						hany.setScaleY(progress/100f);
					}
				}
			}
		});
		rLemak.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Rectangle r = new Rectangle(rLemak.getX(), rLemak.getY(), rLemak.getWidth(), rLemak.getHeight());
				if (r.contains(lemak[1].getX(), lemak[1].getY())) {
					lemak[1].setY(480 - lemak[1].getHeight() - 115);
					if (status == "belom"){
						progress += POINT_SIZE;
						hany.setScaleY(progress/100f);
					}
				}
			}
		});
		
		//PLACING STATIC IMAGE
		sidebarR.setX(800 - sidebarR.getWidth());
		brain.setX(600);
		brain.setY(328);
		duodenum.setX(150);
		duodenum.setY(480 - duodenum.getHeight());
		rGula.setX(212);
		rGula.setY(480 - rGula.getHeight() - 406);
		rLemak.setX(364);
		rLemak.setY(480 - rLemak.getHeight() - 406);
		rProtein.setX(507);
		rProtein.setY(480 - rProtein.getHeight() - 406);
		left.setX(252);
		left.setY(480 - left.getHeight() - 142);
		center.setX(398);
		center.setY(480 - center.getHeight()- 137);
		right.setX(441);
		right.setY(480 - right.getHeight()- 141);
		
		//PLACING Dynamic IMAGE
		gula[1].setX(267+rGula.getWidth());
		gula[1].setY(480 - gula[1].getHeight() - 115);
		lemak[1].setX(375);
		lemak[1].setY(480 - lemak[1].getHeight() - 115);
		protein[1].setX(413);
		protein[1].setY(480 - protein[1].getHeight() - 115);
		
		stage.addActor(bg);
		stage.addActor(sidebarL);
		stage.addActor(sidebarR);
		stage.addActor(brain);
		stage.addActor(duodenum);
		stage.addActor(left);
		stage.addActor(center);
		stage.addActor(right);
		stage.addActor(gula[1]);
		stage.addActor(lemak[1]);
		stage.addActor(protein[1]);
		stage.addActor(rGula);
		stage.addActor(rLemak);
		stage.addActor(rProtein);
		
		//tutorial
		dmanager = new DialogManager(getAtlas().findRegion("dialogbox"), getAtlas().findRegion("pointer_left"),  getAtlas().findRegion("pointer_right"));
		dmanager.addMsg("Tekan tombol ini", 150, 100);
		dmanager.addMsg("Sesuai dengan bolanya", 150, 200);
		dmanager.init();
		dmanager.setOnFinishAction(new Runnable(){
			@Override
			public void run() {
				status = "belom";
			}
		});
		dmanager.init();
		stage.addActor(dmanager);
		stage.addActor(mulai);
		loadKepala();
		loadInfus();
		hany.setScaleY(progress/100f);
		poison.setScaleY(time/100f);
		loadHelpPauseButton();
		loadPauseScreen();
		
	}
	
	@Override
	public void render(float delta){
		super.render(delta);
		super.updatePause();
		if (status == "menang") {
			System.gc();
			int score;
			int poisons = (int) poison.getScaleY()*100;
			score = (100 - poisons) * 5000;
			game.setArcadeScore(5, score);
			game.setLevelCompleted(6);
			startKepalaAnimation(status, new ScoreScreen(game, "DuabelasScreen"));
		} else if (status == "kalah") {
			System.gc();
			startKepalaAnimation(status, new GameOverScreen(game));
		}
		float m = left.getHeight()/left.getWidth();
		float n = right.getHeight()/right.getWidth();
		if (!getPause() && status == "belom") {			
			//Infus
			if (progress >= 100) {
				status = "menang";
				//game.setScreen(new ModeScreen(game));
			} if (poison.getScaleY() >= 1) {
				status = "kalah";
			}
			if (poison.getScaleY() < 1) {
				time += speed;
				poison.setScaleY((float) (time*0.05/100f));
			}
			
			
			//MASIH JOROK
			//animasi turun blok protein
			if (gula[1].getY() > - gula[1].getHeight()) {
				gula[1].setY((float) (gula[1].getY() - 75*delta*m));
				gula[1].setX(gula[1].getX() - 75*delta);
			} else {
				gula[1].setX(267+rGula.getWidth());
				gula[1].setY(480 - gula[1].getHeight() - 115);
			}
			
			//animasi turun blok lemak
			if (lemak[1].getY() > - lemak[1].getY()) {
				lemak[1].setY(lemak[1].getY() -100*delta);
			} else {
				lemak[1].setY(480 - lemak[1].getHeight() - 115);
			}
			
			//animasi turun blok protein
			if (protein[1].getY() > -protein[1].getY()) {
				protein[1].setY((float) (protein[1].getY() - 90*delta*n));
				protein[1].setX(protein[1].getX() + 90*delta);
			} else {
				protein[1].setX(413);
				protein[1].setY(480 - protein[1].getHeight() - 115);
			}	
		}
	}

}
