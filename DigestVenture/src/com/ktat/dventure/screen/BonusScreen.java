package com.ktat.dventure.screen;

import java.util.ArrayList;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.ktat.dventure.main.DVenture;
import com.ktat.dventure.objects.ImageAnimation;

public class BonusScreen extends AbstractScreen {

	private Image sidebarR, sidebarL, bg, progressbar, progressbar_static;
	private ImageButton pause, help;
	private ImageAnimation brain;
	private int curProgress;
	private ArrayList<ImageAnimation> bacteria;
	private int counter[];
	private final int MAX_BACTERIA = 8;
	
	public BonusScreen(DVenture game) {
		super(game);
		
		sidebarR = new Image(getAtlas().findRegion("sidebar"));
		sidebarL = new Image(getAtlas().findRegion("sidebar"));
		bg = new Image(getAtlas().findRegion("backgroundnew"));
		pause = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("pause_untouched")), new TextureRegionDrawable(getAtlas().findRegion("pause_touched")));
		help = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("help_untouched")), new TextureRegionDrawable(getAtlas().findRegion("help_touched")));
		progressbar = new Image(new Sprite(getAtlas().findRegion("progress_bar")));
		progressbar_static = new Image(getAtlas().findRegion("progress_bar"));
		brain = new ImageAnimation(getAtlas().findRegions("brainleft"), 0.050f);
		
		//bacteries
		counter = new int[MAX_BACTERIA];
		for (int i=0;i<MAX_BACTERIA;i++){
			counter[i] = 0;
		}
		
		bacteria = new ArrayList<ImageAnimation>();
		for (int i=0;i<MAX_BACTERIA;i++){
			bacteria.add(new ImageAnimation(getAtlas().findRegions("bactery"), 0.500f));
			//random placing
			int x = new Random().nextInt(440)+180;
			int y = new Random().nextInt(494);
			bacteria.get(i).setPosition(x, y);
		}
		
		sidebarR.setX(800 - sidebarR.getWidth());
		bg.setX(179);
		progressbar_static.setPosition(20, 120);
		progressbar.setPosition(20, 120);
		brain.setPosition(600, 328);
		help.setPosition(14, 380);
		pause.setPosition(90, 380);
		
		curProgress = 0;
		progressbar.setScaleY(curProgress/100f);
		
		brain.addListener(new ClickListener(){
			public void clicked(InputEvent event, float x, float y){
				curProgress ++;
				System.out.println(curProgress);
			}
		});
		
		for (int i=0;i<MAX_BACTERIA;i++){
			final int j = i;
			bacteria.get(i).addListener(new ClickListener(){
				public void clicked(InputEvent event, float x, float y){
					counter[j]++;
				}
			});
		}
		
		stage.addActor(bg);
		for (int i=0;i<MAX_BACTERIA;i++){
			stage.addActor(bacteria.get(i));
		}
		stage.addActor(sidebarR);
		stage.addActor(sidebarL);
		stage.addActor(progressbar_static);
		stage.addActor(progressbar);
		stage.addActor(brain);
		stage.addActor(help);
		stage.addActor(pause);
	}

	@Override
	public void render(float delta) {
		super.render(delta);
		
		if(Gdx.input.justTouched()){
			progressbar.setScaleY(progressbar.getScaleY() + 15/100f);
		}
		
		for (int i=0;i<MAX_BACTERIA;i++){
			int x = new Random().nextInt(7);
			int y = new Random().nextInt(7);
			x -= 3; y -=3;
			bacteria.get(i).setPosition(bacteria.get(i).getX()+x, bacteria.get(i).getY()+y);
			//cek posisi
			if (bacteria.get(i).getX() > 620){
				bacteria.get(i).setX(620);
			} else if (bacteria.get(i).getX() < 180){
				bacteria.get(i).setX(180);
			}
			
			if (bacteria.get(i).getY() > 494){
				bacteria.get(i).setY(494);
			} else if (bacteria.get(i).getY() < 0){
				bacteria.get(i).setY(0);
			}
		}
		
		//cek bakteri
		for (int i=0;i<MAX_BACTERIA;i++){
			if (counter[i]>=5){
				bacteria.get(i).setVisible(false);
			}
		}
	}
}
