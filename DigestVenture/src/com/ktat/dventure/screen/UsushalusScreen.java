package com.ktat.dventure.screen;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.ktat.dventure.main.DVenture;
import com.ktat.dventure.objects.DialogManager;
import com.ktat.dventure.objects.ImageAnimation;
import com.ktat.dventure.objects.DialogManager.Direction;

public class UsushalusScreen extends AbstractScreen{
	private Image bg, sidebarR, sidebarL, bg_intestine;
	private ImageAnimation brain, bakteri[], sBakteri[];
	private ImageButton protein[], lemak[], gula[];
	private Image sProtein[], sLemak[], sGula[];
	
	private float speed = 3;
	private float progress = 0;
	private float time = 0;
	private String status = "tutorial"; //belom, menang, kalah
	private Image mulai;
	private DialogManager dialog;
	private final float POINT_SIZE = 2.22f;
		
	public UsushalusScreen(final DVenture game) {
		super(game);
		
		brain = new ImageAnimation(getAtlas().findRegions("brainleft"), 0.050f);
		bg_intestine = new Image(getAtlas().findRegion("bg_intestine"));
		bg = new Image(getAtlas().findRegion("bg_game"));
		sidebarR = new Image(getAtlas().findRegion("sidebar"));
		sidebarL = new Image(getAtlas().findRegion("sidebar"));
		mulai = new Image(getAtlas().findRegion("go"));		
		mulai.setPosition((800 - mulai.getWidth())/2, (480 - mulai.getHeight())/2);
		mulai.addAction(Actions.alpha(0f));
		
		bg.setX(150);
		brain.setX(600);
		brain.setY(328);
		bg_intestine.setX(150);
		
		sProtein = new Image[15];
		sLemak = new Image[15];
		sGula = new Image[15];
		sBakteri = new ImageAnimation[15];
		for (int i = 0; i < 10; i++) {
			sBakteri[i] = new ImageAnimation(getAtlas().findRegions("bactery_small"), 0.050f);
			sProtein[i] =  new Image(getAtlas().findRegion("protein_small"));
			sLemak[i] =  new Image(getAtlas().findRegion("fat_small"));
			sGula[i] =  new Image(getAtlas().findRegion("glucose_small"));
			sBakteri[i].setVisible(false);
			sProtein[i].setVisible(false);
			sLemak[i].setVisible(false);
			sGula[i].setVisible(false);
		}
		
		protein = new ImageButton[20];
		lemak = new ImageButton[20];
		gula = new ImageButton[20];
		bakteri = new ImageAnimation[20];
		for (int i = 0; i < 10; i++) {
			protein[i] = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("protein")), new TextureRegionDrawable(getAtlas().findRegion("protein")));
			protein[i].setPosition(650 + protein[i].getWidth() + MathUtils.random(1, 50) , MathUtils.random(50, 450));
			lemak[i] = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("fat")), new TextureRegionDrawable(getAtlas().findRegion("fat")));
			lemak[i].setPosition(650 + protein[i].getWidth() + MathUtils.random(1, 50) , MathUtils.random(50, 450));
			gula[i] = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("glucose")), new TextureRegionDrawable(getAtlas().findRegion("glucose")));
			gula[i].setPosition(650 + protein[i].getWidth() + MathUtils.random(1, 50) , MathUtils.random(50, 450));
			bakteri[i] = new ImageAnimation(getAtlas().findRegions("bactery"), 0.050f);
			bakteri[i].setPosition(650 + bakteri[i].getWidth() + MathUtils.random(1, 50) , MathUtils.random(50, 450));
			final int j = i;
			Actor a = new Actor();
			protein[i].addListener(new ClickListener() {
				@Override
				public void enter(InputEvent event, float x, float y, int pointer, Actor a) {
					protein[j].setVisible(false);
					protein[j].setDisabled(true);
					sProtein[j].setVisible(true);
					sProtein[j].setPosition(protein[j].getX(), protein[j].getY());
					if (status == "belom"){
						progress += POINT_SIZE;
						hany.setScaleY(progress/100f);
					}
				}
			});
			
			lemak[i].addListener(new ClickListener() {
				@Override
				public void enter(InputEvent event, float x, float y, int pointer, Actor a) {
					lemak[j].setVisible(false);
					lemak[j].setDisabled(true);
					sLemak[j].setVisible(true);
					sLemak[j].setPosition(lemak[j].getX(), lemak[j].getY());
					if (status == "belom"){
						progress += POINT_SIZE;
						hany.setScaleY(progress/100f);
					}
				}
			});
			
			gula[i].addListener(new ClickListener() {
				@Override
				public void enter(InputEvent event, float x, float y, int pointer, Actor a) {
					gula[j].setVisible(false);
					gula[j].setDisabled(true);
					sGula[j].setVisible(true);
					sGula[j].setPosition(gula[j].getX(), gula[j].getY());
					if (status == "belom"){
						progress += POINT_SIZE;
						hany.setScaleY(progress/100f);
					}
				}
			});
			
			bakteri[i].addListener(new ClickListener() {
				@Override
				public void enter(InputEvent event, float x, float y, int pointer, Actor a) {
					bakteri[j].setVisible(false);
					sBakteri[j].setVisible(true);
					sBakteri[j].setPosition(bakteri[j].getX(), bakteri[j].getY());
					if (status == "belom"){
						progress -= POINT_SIZE;
						hany.setScaleY(progress/100f);
					}
				}
			});
		}
		
		//SET STAGE
		sidebarR.setX(800 - sidebarR.getWidth());
		stage.addActor(bg);
		for (int i = 0; i < 10; i++) {
			stage.addActor(protein[i]);
			stage.addActor(sProtein[i]);
			stage.addActor(lemak[i]);
			stage.addActor(sLemak[i]);
			stage.addActor(gula[i]);
			stage.addActor(sGula[i]);
		}
		for (int i = 0; i < 5; i++) {
			stage.addActor(bakteri[i]);
			stage.addActor(sBakteri[i]);
		}
		stage.addActor(sidebarL);
		stage.addActor(sidebarR);
		stage.addActor(bg_intestine);
		stage.addActor(brain);
		stage.addActor(mulai);
		
		dialog = new DialogManager(getAtlas().findRegion("dialogbox"), getAtlas().findRegion("pointer_left"),  getAtlas().findRegion("pointer_right"));
		dialog.addMsg("Tekan tombol sesuai dengan bola", 150, 100);
		dialog.setOnFinishAction(new Runnable(){
			@Override
			public void run() {
				mulai.addAction(Actions.sequence(Actions.fadeIn(0.5f), Actions.delay(0.5f), Actions.run(new Runnable(){
					@Override
					public void run() {
						mulai.remove();
						status = "belom";
					}
				})));
			}
		});
		dialog.init();
		
		//tutorial
		dialog = new DialogManager(getAtlas().findRegion("dialogbox"), getAtlas().findRegion("pointer_left"),  getAtlas().findRegion("pointer_right"));
		dialog.addMsg("Slide semua bola yang datang", 150, 250);
		dialog.setOnFinishAction(new Runnable(){
			@Override
			public void run() {
				status = "belom";
			}
		});
		dialog.init();
		
		stage.addActor(dialog);
		
		loadKepala();
		loadInfus();
		hany.setScaleY(progress/100f);
		poison.setScaleY(time/100f);
		stage.addActor(dialog);
		loadHelpPauseButton();
		loadPauseScreen();
	}
	
	@Override
	public void render(float delta){
		super.render(delta);
		super.updatePause();
		if (status == "menang") {
			System.gc();
			int score;
			int poisons = (int) (poison.getScaleY()*100f);
			score = poisons * 50;
			game.setArcadeScore(6, score);
			game.setLevelCompleted(7);
			startKepalaAnimation(status, new ScoreScreen(game, "UsushalusScreen"));
		} else if (status == "kalah") {
			System.gc();
			startKepalaAnimation(status, new GameOverScreen(game));
		}
		if (!getPause() && status == "belom") {			
			//Infus
			if (progress >= 100) {
				status = "menang";
				//game.setScreen(new ModeScreen(game));
			} if (poison.getScaleY() >= 1) {
				status = "kalah";
			}
			if (poison.getScaleY() < 1) {
				time += speed;
				poison.setScaleY((float) (time*0.05/100f));
			}
			for (int i = 0; i < 10; i++) {
				//==================PROTEIN=================== 
				//BIG MODE 
				if (protein[i].isVisible()) {
					if (protein[i].isVisible() && protein[i].getX() > 150) {
						protein[i].setX(protein[i].getX() - MathUtils.random(50, 400)*delta);
					} else {
						protein[i].setPosition(650 + protein[i].getWidth() + MathUtils.random(1, 50) , MathUtils.random(50, 450));
					}
				//SMALL MODE
				} else {
					if (sProtein[i].getY() > -sProtein[i].getHeight()) {
						sProtein[i].setY(sProtein[i].getY() - MathUtils.random(50, 400)*delta);
					} else {
						protein[i].setVisible(true);
						protein[i].setPosition(650 + protein[i].getWidth() + MathUtils.random(1, 50) , MathUtils.random(50, 450));
					}
				}
				
				
				//==================LEMAK=================== 
				//BIG MODE
				if (lemak[i].isVisible()) {
					if (lemak[i].isVisible() && lemak[i].getX() > 150) {
						lemak[i].setX(lemak[i].getX() - MathUtils.random(50, 400)*delta);
					} else {
						lemak[i].setPosition(650 + lemak[i].getWidth() + MathUtils.random(1, 50) , MathUtils.random(50, 450));
					}
				//SMALL MODE
				} else {
					if (sLemak[i].getY() > -sLemak[i].getHeight()) {
						sLemak[i].setY(sLemak[i].getY() - MathUtils.random(50, 400)*delta);
					} else {
						lemak[i].setVisible(true);
						lemak[i].setPosition(650 + lemak[i].getWidth() + MathUtils.random(1, 50) , MathUtils.random(50, 450));
					}
				}
				
				
				//==================GULA=================== 
				//BIG MODE 
				if (gula[i].isVisible()) {
					if (gula[i].isVisible() && gula[i].getX() > 150) {
						gula[i].setX(gula[i].getX() - MathUtils.random(50, 400)*delta);
					} else {
						gula[i].setPosition(650 + gula[i].getWidth() + MathUtils.random(1, 50) , MathUtils.random(50, 450));
					}
				//SMALL MODE
				} else {
					if (sGula[i].getY() > -sGula[i].getHeight()) {
						sGula[i].setY(sGula[i].getY() - MathUtils.random(50, 400)*delta);
					} else {
						gula[i].setVisible(true);
						gula[i].setPosition(650 + gula[i].getWidth() + MathUtils.random(1, 50) , MathUtils.random(50, 450));
					}
				}
				
				
				//==================BAKTERI=================== 
				//BIG MODE 
				if (bakteri[i].isVisible()) {
					if (bakteri[i].isVisible() && bakteri[i].getX() > 150) {
						bakteri[i].setX(bakteri[i].getX() - MathUtils.random(50, 400)*delta);
					} else {
						bakteri[i].setPosition(650 + bakteri[i].getWidth() + MathUtils.random(1, 50) , MathUtils.random(50, 450));
					}
				//SMALL MODE
				} else {
					if (sBakteri[i].getY() > -sBakteri[i].getHeight()) {
						sBakteri[i].setY(sBakteri[i].getY() - MathUtils.random(50, 400)*delta);
					} else {
						bakteri[i].setVisible(true);
						bakteri[i].setPosition(650 + bakteri[i].getWidth() + MathUtils.random(1, 50) , MathUtils.random(50, 450));
					}
				}
			}
		}
	}			
}
