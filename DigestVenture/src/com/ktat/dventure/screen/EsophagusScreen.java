package com.ktat.dventure.screen;


import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.ktat.dventure.main.DVenture;
import com.ktat.dventure.objects.DialogManager;
import com.ktat.dventure.objects.DialogManager.Direction;
import com.ktat.dventure.objects.ImageAnimation;

public class EsophagusScreen extends AbstractScreen{
	private Image sidebarR, sidebarL, bg;
	private ImageAnimation brain;
	private float progress = 0;
	private float time = 0;
	private float speed = 3;
	private float counterBar = 0;
	private String status = "tutorial";
	private DialogManager dialog;
	
	private Image chyme, esofagus, track;
	private int chymePosition;
	private ImageButton buttonTrack, buttonTap;
	private boolean counted = false;
	private boolean tapEnabled = false;
	private final int POINT_SIZE = 20;
	private Image mulai;
	
	public EsophagusScreen(DVenture game) {
		super(game);
		//Images
		sidebarR = new Image(getAtlas().findRegion("sidebar"));
		sidebarL = new Image(getAtlas().findRegion("sidebar"));
		bg = new Image(getAtlas().findRegion("bg_game"));
		bg.setX(150);
		brain = new ImageAnimation(getAtlas().findRegions("brainleft"), 0.050f);
		bg = new Image(getAtlas().findRegion("background"));
		esofagus = new Image(getAtlas().findRegion("bg_esophagus"));
		track = new Image(getAtlas().findRegion("slide_red"));
		mulai = new Image(getAtlas().findRegion("go"));		
		mulai.setPosition((800 - mulai.getWidth())/2, (480 - mulai.getHeight())/2);
		mulai.addAction(Actions.alpha(0f));
		
		//Buttons
		buttonTrack = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("button_red_unpressed")), new TextureRegionDrawable(getAtlas().findRegion("button_red_pressed")));
		buttonTrack.setPosition(680, 100);
		track.setPosition(680, 100);
		
		buttonTap = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("button_green_unpressed")), new TextureRegionDrawable(getAtlas().findRegion("button_green_pressed")));
		buttonTap.setPosition(680, 200);
		buttonTap.setVisible(false);
		
		//button listeners
		buttonTrack.addListener(new DragListener() {
			@Override
			public void drag(InputEvent event, float x, float y, int pointer) {
				super.drag(event, x, y, pointer);
				Actor actor = event.getTarget();
				actor.setY(y-actor.getHeight()/2);
				counter(x,y);
			}
		});
		
		buttonTap.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				if (tapEnabled){
					tapEnabled = false;
					buttonTrack.setPosition(680, 100);
					buttonTrack.setVisible(true);
					buttonTap.setVisible(false);
					track.setVisible(true);
					if (status == "belom"){
						progress += POINT_SIZE;
						hany.setScaleY(progress/100f);
					}
				}
			}
		});
		
		//Esofagus
		esofagus.setPosition(350, 0);
		chyme = new Image(getAtlas().findRegion("contracting_esofagus"));
		chymePosition = 5;
		chyme.setPosition(350, chymePosition*80);
		
		//image positioning
		sidebarR.setX(800 - sidebarR.getWidth());
		bg.setX(179);
		brain.setPosition(600, 328);
		
		//Staging Actors
		stage.addActor(bg);
		stage.addActor(sidebarR);
		stage.addActor(sidebarL);
		stage.addActor(brain);
		stage.addActor(esofagus);
		stage.addActor(chyme);
		stage.addActor(track);
		stage.addActor(buttonTrack);
		stage.addActor(buttonTap);
		stage.addActor(mulai);
		
		loadKepala();
		loadInfus();
		hany.setScaleY(progress/100f);
		poison.setScaleY(time/100f);
		loadHelpPauseButton();
		loadPauseScreen();
		
		//tutorial
		dialog = new DialogManager(getAtlas().findRegion("dialogbox"), getAtlas().findRegion("pointer_left"),  getAtlas().findRegion("pointer_right"));
		dialog.addMsg("Slide tombol ini keatas", 150, 150);
		dialog.addMsg("Lalu tap tombol hijau untuk mendorong\nmakanan", 150, 150);
		dialog.setOnFinishAction(new Runnable(){
			@Override
			public void run() {
				mulai.addAction(Actions.sequence(Actions.fadeIn(0.5f), Actions.delay(0.5f), Actions.run(new Runnable(){
					@Override
					public void run() {
						mulai.remove();
						status = "belom";
					}
				})));
			}
		});
		dialog.init();
		stage.addActor(dialog);
	}
	
	public void render(float delta){
		super.render(delta);
		super.updatePause();
		if (status == "menang") {
			System.gc();
			int score;
			int poisons = (int) (poison.getScaleY()*100f);
			score = poisons * 50;
			game.setArcadeScore(3, score);
			game.setLevelCompleted(4);
			startKepalaAnimation(status, new ScoreScreen(game, "EsophagusScreen"));
		} else if (status == "kalah") {
			System.gc();
			startKepalaAnimation(status, new GameOverScreen(game));
		}
		if (!getPause() && status == "belom") {
			//Infus
			if (progress >= 100) {
				progress = 100;
				status = "menang";
				//game.setScreen(new ModeScreen(game));
			} else {
				hany.setScaleY(progress/100f);
			}
			
			if (poison.getScaleY() >= 1) {
				status = "kalah";
			}
			if (poison.getScaleY() < 1) {
				time += speed;
				poison.setScaleY((float) (time*0.05/100f));
			}
			
			if (chyme.getY() > (((100-progress)/100f*280)+100)){
				chyme.setY(chyme.getY()-50*delta);
			}
			/*
			if (counterBar < progress){
				counterBar += 0.5f;
			} else if (counterBar > progress){
				counterBar -= 1;
			}
			*/
		}
	}
	
	public void counter(float x, float y){
		if (!counted){
			if (y > 200){
				counted = true;
				setTap();
			}	
		}
	}

	private void setTap() {
		buttonTrack.setDisabled(true);
		buttonTrack.setVisible(false);
		counted = false;
		tapEnabled = true;
		buttonTap.setVisible(true);
		track.setVisible(false);
	}

}
