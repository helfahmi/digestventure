package com.ktat.dventure.screen;

import java.io.IOException;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.ktat.dventure.main.DVenture;
import com.ktat.dventure.objects.FunFact;
import com.ktat.dventure.objects.ImageAnimation;

public class ScoreScreen extends AbstractScreen{
	private Image bg;
	private int totalScore, baseScore, stageScore;
	private Label totalLabel, baseLabel, stageLabel, totalText, baseText, stageText;
	private boolean looped = false;
	private final String totalString = "Total Score ";
	private final String baseString = "Base Score ";
	private final String stageString = "Stage Score ";
	private final int bronzeRange = 2000;
	private final int silverRange = 3000;
	private final int goldRange = 4000;
	private String pScreen, fact;
	private ImageButton next;
	private FunFact facts;
	private Image funBox;
	private Label label;
	private ImageAnimation figure;

	public ScoreScreen(final DVenture game, final String pScreen) {
		super(game);
		//Background images
		bg = new Image(getAtlas().findRegion("background"));
		next = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("play_untouched")), new TextureRegionDrawable(getAtlas().findRegion("play_touched")));
		next.setPosition(800 - next.getWidth() - 15, 15);
		this.pScreen = pScreen;
		next.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (pScreen == "TongueScreen") 
					fadeOutAndChangeTo(new GigiScreen(game));
				if (pScreen == "GigiScreen") 
					fadeOutAndChangeTo(new AmilaseScreen(game));
				if (pScreen == "AmilaseScreen") 
					fadeOutAndChangeTo(new EsophagusScreen(game));
				if (pScreen == "EsophagusScreen") 
					fadeOutAndChangeTo(new StomachScreen(game));
				if (pScreen == "StomachScreen") 
					fadeOutAndChangeTo(new DuabelasScreen(game));
				if (pScreen == "DuabelasScreen") 
					fadeOutAndChangeTo(new UsushalusScreen(game));
				if (pScreen == "UsushalusScreen") 
					fadeOutAndChangeTo(new LargeIntestineScreen(game));
				if (pScreen == "LargeIntestineScreen"){ 
					if(game.getMode() == "story")
						fadeOutAndChangeTo(new FinishedScreenDark(game));
					else
						fadeOutAndChangeTo(new MapScreen(game));
				}
			}
		});
		
		//facts
		try {
			facts = new FunFact();
		} catch (IOException e) {
			e.printStackTrace();
		}
		int prevStage = 0;
		if (pScreen == "TongueScreen"){ 
			prevStage = 0;
			figure = new ImageAnimation(getAtlas().findRegions("brainright"), 0.05f);
		}
		if (pScreen == "GigiScreen"){ 
			prevStage = 0;
			figure = new ImageAnimation(getAtlas().findRegions("brainright"), 0.05f);
		}
		if (pScreen == "AmilaseScreen"){ 
			prevStage = 1;
			figure = new ImageAnimation(getAtlas().findRegions("brainright"), 0.05f);
		}
		if (pScreen == "EsophagusScreen"){ 
			prevStage = 2;
			figure = new ImageAnimation(getAtlas().findRegions("esophagusright"), 0.05f);
		}
		if (pScreen == "StomachScreen"){
			prevStage = 3;
			figure = new ImageAnimation(getAtlas().findRegions("stomachright"), 0.05f);
		}
		if (pScreen == "DuabelasScreen"){ 
			prevStage = 4;
			figure = new ImageAnimation(getAtlas().findRegions("duodenumright"), 0.05f);
		}
		if (pScreen == "UsushalusScreen"){ 
			prevStage = 5;
			figure = new ImageAnimation(getAtlas().findRegions("intestine"), 0.05f);
		}
		if (pScreen == "LargeIntestineScreen"){ 
			prevStage = 6;
			figure = new ImageAnimation(getAtlas().findRegions("colon"), 0.05f);
		}
		if (pScreen == "FinishedScreenDark"){ 
			prevStage = 7;
			figure = new ImageAnimation(getAtlas().findRegions("colon"), 0.05f);
		}
		
		funBox = new Image(getAtlas().findRegion("factbox"));
		funBox.setPosition(40, 370);
		fact = facts.getFact(prevStage);
		label = new Label(fact, getSkin());
		label.setFontScale(0.2f);
		label.setPosition(100, 385);
		figure.setPosition(50, 50);
		
		//Scores
		baseScore = 1500;
		stageScore = game.getArcadeScore(prevStage);
		totalScore = baseScore+stageScore;
		
		//Draw labels		
		totalLabel = new Label(Integer.toString(totalScore), getSkin());
		baseLabel = new Label(Integer.toString(baseScore), getSkin());
		stageLabel = new Label(Integer.toString(stageScore), getSkin());
		
		totalText = new Label(totalString, getSkin());
		baseText = new Label(baseString, getSkin());
		stageText = new Label(stageString, getSkin());
		
		//placing labels
		totalLabel.setPosition(250, 50);
		totalLabel.setFontScale(1.8f);
		if (totalScore > silverRange){
			totalLabel.setColor(Color.YELLOW);
		} else if (totalScore > goldRange){
			totalLabel.setColor(Color.RED);
		}
		baseLabel.setPosition(450, 300);
		baseLabel.setFontScale(0.6f);
		stageLabel.setPosition(450, 250);
		stageLabel.setFontScale(0.6f);
		
		totalText.setPosition(100, 180);
		totalText.setFontScale(0.8f);
		baseText.setPosition(300, 300);
		baseText.setFontScale(0.3f);
		stageText.setPosition(300, 250);
		stageText.setFontScale(0.3f);
		
		//staging
		stage.addActor(bg);
		stage.addActor(totalLabel);
		stage.addActor(baseLabel);
		stage.addActor(stageLabel);
		stage.addActor(totalText);
		stage.addActor(baseText);
		stage.addActor(stageText);
		stage.addActor(next);
		stage.addActor(funBox);
		stage.addActor(label);
		stage.addActor(figure);
		
		baseLabel.addAction(Actions.sequence(Actions.alpha(0), Actions.delay(0.5f), Actions.fadeIn(1f)));
		stageLabel.addAction(Actions.sequence(Actions.alpha(0), Actions.delay(0.5f), Actions.fadeIn(1f), Actions.scaleTo(1.1f, 1.1f, 0.5f), Actions.scaleTo(1f, 1f, 0.5f)));
		totalText.addAction(Actions.sequence(Actions.alpha(0), Actions.delay(1.2f), Actions.fadeIn(1f)));
		totalLabel.addAction(Actions.sequence(Actions.alpha(0), Actions.delay(1.5f), Actions.fadeIn(1f)));
		next.addAction(Actions.sequence(Actions.alpha(0), Actions.delay(2f), Actions.fadeIn(1f)));
		figure.addAction(Actions.sequence(Actions.alpha(0), Actions.delay(1.5f), Actions.fadeIn(1f)));
	}
	
	public void render(float delta){
		super.render(delta);
		/*if (!looped){
			if (totalText.getX() < 300){
				totalText.setX(totalText.getX()+80*delta);
			} else {
				if (totalText.getX() < 350){
					totalText.setX(totalText.getX()+20*delta);
				} else {
					if (totalText.getX() > 400){
						looped = true;
					} 
				}
			}
		}*/
	}

}
