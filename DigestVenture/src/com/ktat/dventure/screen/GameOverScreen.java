package com.ktat.dventure.screen;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.ktat.dventure.main.DVenture;

public class GameOverScreen extends AbstractScreen {

	private Image bg;
	private Image title;
	private ImageButton retry, quit; 
	
	public GameOverScreen(final DVenture game) {
		super(game);
		// TODO Auto-generated constructor stub
		
		bg = new Image(getAtlas().findRegion("background"));
		quit = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("quit_untouched")), new TextureRegionDrawable(getAtlas().findRegion("quit_touched")));
		quit.setPosition(800 - quit.getWidth() - 15, 15);
		title = new Image(getAtlas().findRegion("gameover_title"));
		title.setPosition((800 - title.getWidth())/2, 270);
		quit.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (game.getMode().equals("story")){
					game.pauseStory();
				} else {
					game.pauseArcade();
				}
				fadeOutAndChangeTo(new MenuScreen(game));
			}
		});
		stage.addActor(bg);
		stage.addActor(quit);
		stage.addActor(title);
		
	}
	
	public void render(float delta){
		super.render(delta);
	}

}
