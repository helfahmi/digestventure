package com.ktat.dventure.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop.Payload;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop.Source;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop.Target;
import com.ktat.dventure.main.DVenture;
import com.ktat.dventure.objects.Image2;

public class TestScreen extends AbstractScreen{
	private Image test;
	private Label label, label1, label2;
	public TestScreen(DVenture game) {
		super(game); //Harus ada
		
		/* Dari atlas yang sudah ada, ambil region atlas
		 * yang bernama "12jari" (nama file aslinya, ekstensi nggak perlu diikutkan),
		 * pass sebagai parameter ke class Image (extends Actor, library dari libGDX) 
		 * untuk membuat Actor yang memiliki gambar.
		 * */
		//background = new Image(getAtlas().findRegion("background")); 
		
		/* Kalo mau ngubah-ngubah properti sebelum dimasukkan ke Stage, ubah aja langsung lewat:
		 * background.setPosition(x, y), dst.
		 * */
		
		/* Masukkan gambar background ke stage */
		//stage.addActor(background);
		
		/* Karena render & draw stage sudah ditangani oleh class AbstractScreen,
		 * gambar langsung terlihat di screen! 
		 * */
		
		/* Kalau ingin class Actor yang bertindak lain (dan punya properti gambar)
		 * silahkan buat class yang "extends Image", lalu @Override method
		 * act(float delta) dan/atau draw(SpriteBatch batch, float parentAlpha) yang 
		 * dimiliki oleh class Image.
		 * */
		Color c = Color.WHITE;
		//setBackgroundColor(c.r, c.g, c.b);
		
		final Skin skin = new Skin(Gdx.files.internal("skins/menuskin.json"));
		label = new Label("Payload source", skin);
		label1 = new Label("valid target", skin);
		label1.setX(430);
		label1.setY(300);
		label2 = new Label("invalid target", skin);
		label2.setPosition(400, 400);
//		label.setFontScale(0.2f);
//		test = new Image(getAtlas().findRegion("banana"));
//		test.setSize(test.getWidth(), 300);
//		//setBackgroundColor(r, g, b)
//		test.setPosition(0, 480 - test.getHeight());
//		stage.addActor(test);
//		stage.addActor(label);
		
		
		
		DragAndDrop dragAndDrop = new DragAndDrop();
		dragAndDrop.addSource(new Source(label) {
			public Payload dragStart (InputEvent event, float x, float y, int pointer) {
				Payload payload = new Payload();
				payload.setObject("Some payload!");

				payload.setDragActor(new Label("Some payload!", skin));

				Label validLabel = new Label("Some payload!", skin);
				validLabel.setColor(0, 1, 0, 1);
				payload.setValidDragActor(validLabel);

				Label invalidLabel = new Label("Some payload!", skin);
				invalidLabel.setColor(1, 0, 0, 1);
				payload.setInvalidDragActor(invalidLabel);

				return payload;
			}
		});
		dragAndDrop.addTarget(new Target(label1) {
			public boolean drag (Source source, Payload payload, float x, float y, int pointer) {
				getActor().setColor(Color.GREEN);
				return true;
			}

			public void reset (Source source, Payload payload) {
				getActor().setColor(Color.WHITE);
			}

			public void drop (Source source, Payload payload, float x, float y, int pointer) {
				System.out.println("Accepted: " + payload.getObject() + " " + x + ", " + y);
			}
		});
		dragAndDrop.addTarget(new Target(label2) {
			public boolean drag (Source source, Payload payload, float x, float y, int pointer) {
				getActor().setColor(Color.RED);
				return false;
			}

			public void reset (Source source, Payload payload) {
				getActor().setColor(Color.WHITE);
			}

			public void drop (Source source, Payload payload, float x, float y, int pointer) {
			}
		});
		stage.addActor(label);
		stage.addActor(label1);
		stage.addActor(label2);
	}
	
	@Override
	public void render(float delta){
		super.render(delta);
	}

}
