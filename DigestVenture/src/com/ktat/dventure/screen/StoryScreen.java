package com.ktat.dventure.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.ktat.dventure.main.DVenture;
import com.ktat.dventure.objects.DialogManager;
import com.ktat.dventure.objects.DialogManager.Direction;
import com.ktat.dventure.objects.ImageAnimation;

public class StoryScreen extends AbstractScreen{
	
	private Image bg, table, plate, burger, childSmall, childBig, childHand, shadow;
	private ImageButton yes, no;
	private ImageAnimation brain;
	private DialogManager manager;
	private Label yesLabel, noLabel;
	private int state;
	private Label skip;
	//music
	public static Music storyMusic;
	
	public StoryScreen(final DVenture game) {
		super(game);
		
		state = 1;
		skip = new Label("Skip >>", getSkin());
		skip.setFontScale(0.5f);
		skip.setPosition(10, 0);
		brain = new ImageAnimation(getAtlas().findRegions("brainleft"), 0.090f);
		manager = new DialogManager(getAtlas().findRegion("dialogbox"), getAtlas().findRegion("pointer_left"), getAtlas().findRegion("pointer_right"));
		
		yes = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("button_green_unpressed")), new TextureRegionDrawable(getAtlas().findRegion("button_green_pressed")));
		no = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("button_red_unpressed")), new TextureRegionDrawable(getAtlas().findRegion("button_red_pressed")));
		
		bg = new Image(getAtlas().findRegion("background"));
		table = new Image(getAtlas().findRegion("table"));
		plate = new Image(getAtlas().findRegion("plate"));
		burger = new Image(getAtlas().findRegion("hamburger"));
		childSmall = new Image(getAtlas().findRegion("child_small"));
		childBig = new Image(getAtlas().findRegion("child_large"));
		shadow = new Image(getAtlas().findRegion("shadow"));
		childHand = new Image(getAtlas().findRegion("child_large_hand"));
		
		yesLabel = new Label("Ya", getSkin());
		noLabel = new Label("Tidak", getSkin());
		yesLabel.setFontScale(0.5f);
		noLabel.setFontScale(0.5f);
		
		yesLabel.setPosition(169, 105);
		noLabel.setPosition(393, 105);
		yes.setPosition(155, 190);
		no.setPosition(400, 190);
		yes.setDisabled(true);
		no.setDisabled(true);
		
		plate.setPosition((800 - plate.getWidth())/2, 77);
		burger.setPosition(333, 90);
		childSmall.setPosition(800, 0);
		
		childBig.addAction(Actions.alpha(0f));
		childHand.addAction(Actions.alpha(0f));
		brain.addAction(Actions.alpha(0f));
		yes.addAction(Actions.alpha(0f));
		no.addAction(Actions.alpha(0f));
		yesLabel.addAction(Actions.alpha(0f));
		noLabel.addAction(Actions.alpha(0f));
		
		brain.setPosition(588, 259);
		childHand.setY(-100);
		
		skip.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				fadeOutAndChangeTo(new TongueScreen(game));
			}
		});
		
		yes.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				manager.reset();
				manager.remove();
				onChooseYes1();
			}
		});
		
		no.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				manager.reset();
				manager.remove();
				if(getCurState() == 1)
					onChooseNo1();
				else if(getCurState() == 2)
					onChooseNo2();
			}
		});
		
		stage.addActor(bg);
		stage.addActor(yesLabel);
		stage.addActor(noLabel);
		stage.addActor(table);
		stage.addActor(plate);
		stage.addActor(burger);	
		stage.addActor(childSmall);
		stage.addActor(childBig);
		stage.addActor(childHand);
		stage.addActor(brain);
		stage.addActor(yes);
		stage.addActor(no);
		stage.addActor(skip);
		
		Runnable onDelayFinish = new Runnable(){
			@Override
			public void run() {
				moveTableLeft();
			}
		};
		
		stage.addAction(Actions.sequence(Actions.delay(0.5f), Actions.run(onDelayFinish)));
	}
	
	public void onChooseYes2() {
		
	}
	
	public void onChooseNo2() {
		
	}

	public int getCurState(){
		return state;
	}
	
	public void moveTableLeft(){
		Runnable moveChild = new Runnable(){
			@Override
			public void run() {
				moveChildLeft();
			}
		};
		
		table.addAction(Actions.moveTo(table.getX() - 212, table.getY(), 2f));
		plate.addAction(Actions.moveTo(plate.getX() - 212, plate.getY(), 2f));
		burger.addAction(Actions.moveTo(burger.getX() - 212, burger.getY(), 2f));
		stage.addAction(Actions.sequence(Actions.delay(2f), Actions.run(moveChild)));
	}
	
	public void moveChildLeft(){
		Runnable eatFood = new Runnable(){
			@Override
			public void run() {
				startEating();
			}
		};
		
		childSmall.addAction(Actions.sequence(Actions.moveTo(180, 0, 3f), Actions.delay(0.5f), Actions.fadeOut(1f)));
		table.addAction(Actions.sequence(Actions.delay(3.5f), Actions.fadeOut(1f)));
		plate.addAction(Actions.sequence(Actions.delay(3.5f), Actions.fadeOut(1f)));
		burger.addAction(Actions.sequence(Actions.delay(3.5f), Actions.fadeOut(1f), Actions.delay(1f), Actions.run(eatFood)));
	}
	
	public void startEating(){
		Runnable onFinish = new Runnable(){
			@Override
			public void run() {
				brainScene();
			}
		};
		
		childBig.addAction(Actions.sequence(Actions.fadeIn(2f), Actions.delay(1.5f), Actions.fadeOut(1f)));
		childHand.addAction(Actions.sequence(Actions.fadeIn(2f), Actions.parallel(Actions.moveTo(0, childHand.getY() + 30, 1f), Actions.rotateBy(10f, 1f)), Actions.delay(0.5f), Actions.fadeOut(1f), Actions.run(onFinish)));
	}
	
	public void brainScene(){
		Runnable onFinish = new Runnable() {
			@Override
			public void run() {
				showDialogManager();
			}
		};
		brain.addAction(Actions.sequence(Actions.fadeIn(1f), Actions.run(onFinish)));	
	}
	
	public void showDialogManager(){
		manager.addMsg("!!!", 50, 263);
		manager.addMsg("Ups, aku lupa cara mencerna! :(", 50, 263);
		manager.addMsg("Maukah kau membantu anak kecil ini\nmencerna?", 50, 263, new Runnable(){
			@Override
			public void run() {
				showButton();
			}
		});
		manager.init();
		stage.addActor(manager);
	}
	
	public void showButton(){
		yes.setDisabled(false);
		if(state == 1)
			no.setDisabled(false);
		
		yes.addAction(Actions.fadeIn(1f));
		if(state == 1)
			no.addAction(Actions.fadeIn(1f));
		yesLabel.addAction(Actions.fadeIn(1f));
		if(state == 1)
			noLabel.addAction(Actions.fadeIn(1f));
	}
	
	public void onChooseYes1(){
		state = 2;
		
		yes.addAction(Actions.fadeOut(1f));
		no.addAction(Actions.fadeOut(1f));
		yesLabel.addAction(Actions.fadeOut(1f));
		noLabel.addAction(Actions.fadeOut(1f));
		
		manager.addMsg("Terima kasih!", 50, 263);
		manager.addMsg("Tenang saja, aku akan memandumu\nselama perjalanan.", 50, 263);
		manager.addMsg("Yuk mulai!", 50, 263);
		manager.init();
		manager.setOnFinishAction(new Runnable(){
			@Override
			public void run() {
				fadeOutAndChangeTo(new TongueScreen(game));
			}
		});
		stage.addActor(manager);
	}
	
	public void onChooseNo1(){
		state = 2;
		
		yes.addAction(Actions.fadeOut(1f));
		no.addAction(Actions.fadeOut(1f));
		yesLabel.addAction(Actions.fadeOut(1f));
		noLabel.addAction(Actions.fadeOut(1f));
		
		manager.addMsg("Ayolaaaaah...", 50, 263);
		manager.init();
		manager.setOnFinishAction(new Runnable(){
			@Override
			public void run() {
				showButton();
			}
		});
		stage.addActor(manager);
	}
}
