package com.ktat.dventure.screen;

import sun.rmi.runtime.Log;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.ktat.dventure.main.DVenture;
import com.ktat.dventure.objects.DialogManager;
import com.ktat.dventure.objects.DialogManager.Direction;
import com.ktat.dventure.objects.ImageAnimation;
import com.ktat.dventure.objects.DialogManager.Direction;

public class GigiScreen extends AbstractScreen{
	private Image mangap, mingkem, bg, sidebarR, sidebarL;
	private ImageButton tap;
	private ImageAnimation brain;
	private boolean isMingkem = false;
	private DialogManager dialog;
	
	private float speed = 3;
	private float progress = 0;
	private float time = 0;
	private String status = "tutorial"; //belom, menang, kalah
	private final float POINT_SIZE = 3.125f;
	private DialogManager dmanager;
	private Image mulai;
	
	
	public GigiScreen(final DVenture game) {
		super(game);
		
		brain = new ImageAnimation(getAtlas().findRegions("brainleft"), 0.050f);
		tap = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("tap_untouched")), new TextureRegionDrawable(getAtlas().findRegion("tap_touched")));
		bg = new Image(getAtlas().findRegion("bg_game"));
		bg.setX(150);
		mangap = new Image(getAtlas().findRegion("mangap"));
		mingkem = new Image(getAtlas().findRegion("mingkem"));
		sidebarR = new Image(getAtlas().findRegion("sidebar"));
		sidebarL = new Image(getAtlas().findRegion("sidebar"));
		mulai = new Image(getAtlas().findRegion("go"));		
		mulai.setPosition((800 - mulai.getWidth())/2, (480 - mulai.getHeight())/2);
		mulai.addAction(Actions.alpha(0f));
		
		sidebarR.setX(800 - sidebarR.getWidth());
		
		tap.addListener(new ClickListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				isMingkem = true;
				return true;
			}
			
			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				if (status == "belom"){
					progress += POINT_SIZE;
					hany.setScaleY(progress/100f);
				}
				isMingkem = false;
			}
		});
		
		
		brain.setX(600);
		brain.setY(328);
		
		tap.setX(650);
		tap.setY(100);
		
		mangap.setX(250);
		mangap.setY(95);
		mingkem.setX(250);
		mingkem.setY(95);
		mingkem.setVisible(isMingkem);
		
		
		stage.addActor(bg);
		stage.addActor(sidebarL);
		stage.addActor(sidebarR);
		stage.addActor(brain);
		stage.addActor(tap);
		stage.addActor(mangap);
		stage.addActor(mingkem);
		stage.addActor(mulai);
		
		dmanager = new DialogManager(getAtlas().findRegion("dialogbox"), getAtlas().findRegion("pointer_left"),  getAtlas().findRegion("pointer_right"));
		dmanager.addMsg("Tap tombol ini secepat-cepatnya.", 116, 330, 565, 152, Direction.RIGHT);
		dmanager.setOnFinishAction(new Runnable(){
			@Override
			public void run() {
				mulai.addAction(Actions.sequence(Actions.fadeIn(0.5f), Actions.delay(0.5f), Actions.run(new Runnable(){
					@Override
					public void run() {
						mulai.remove();
						status = "belom";
					}
				})));
			}
		});
		dmanager.init();
		
		loadKepala();
		loadInfus();
		hany.setScaleY(progress/100f);
		poison.setScaleY(time/100f);
		loadHelpPauseButton();
		stage.addActor(dmanager);
		loadPauseScreen();
	}
	
	@Override
	public void render(float delta){
		super.render(delta);
		super.updatePause();
		if (status == "menang") {
			System.gc();
			//Diisi nomer level dia + 1
			int score;
			int poisons = (int) (poison.getScaleY()*100f);
			score = poisons * 50;
			game.setArcadeScore(1, score);
			game.setLevelCompleted(2);
			startKepalaAnimation(status, new ScoreScreen(game, "GigiScreen"));
		} else if (status == "kalah") {
			System.gc();
			startKepalaAnimation(status, new GameOverScreen(game));
		}
		if (!getPause() && status == "belom") {
			mingkem.setVisible(isMingkem);
			mangap.setVisible(!isMingkem);
			
			//Infus
			if (progress >= 100)
				status = "menang";
			if (poison.getScaleY() >= 1)
				status = "kalah";
			if(poison.getScaleY() < 1) {
				time += speed;
				poison.setScaleY((float) (time*0.05/100f));
			}
		}
	}

}
