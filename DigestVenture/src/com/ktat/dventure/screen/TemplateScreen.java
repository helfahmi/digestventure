package com.ktat.dventure.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.ktat.dventure.main.DVenture;
import com.ktat.dventure.objects.ImageAnimation;

public class TemplateScreen extends AbstractScreen{
	
	private Image sidebarR, sidebarL, bg, progressbar, progressbar_static;
	private ImageButton pause, help;
	private ImageAnimation brain;
	private int curProgress;
	
	public TemplateScreen(DVenture game) {
		super(game);
		
		sidebarR = new Image(getAtlas().findRegion("sidebar"));
		sidebarL = new Image(getAtlas().findRegion("sidebar"));
		bg = new Image(getAtlas().findRegion("backgroundnew"));
		pause = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("pause_untouched")), new TextureRegionDrawable(getAtlas().findRegion("pause_touched")));
		help = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("help_untouched")), new TextureRegionDrawable(getAtlas().findRegion("help_touched")));
		progressbar = new Image(new Sprite(getAtlas().findRegion("progress_bar")));
		progressbar_static = new Image(getAtlas().findRegion("progress_bar"));
		brain = new ImageAnimation(getAtlas().findRegions("brain"), 0.050f);
		
		sidebarR.setX(800 - sidebarR.getWidth());
		bg.setX(179);
		progressbar_static.setPosition(20, 120);
		progressbar.setPosition(20, 120);
		brain.setPosition(600, 328);
		help.setPosition(14, 380);
		pause.setPosition(90, 380);
		
		curProgress = 20;
		progressbar.setScaleY(curProgress/100f);
		
		stage.addActor(bg);
		stage.addActor(sidebarR);
		stage.addActor(sidebarL);
		stage.addActor(progressbar_static);
		stage.addActor(progressbar);
		stage.addActor(brain);
		stage.addActor(help);
		stage.addActor(pause);
	}
	
	@Override
	public void render(float delta) {
		super.render(delta);
		
		if(Gdx.input.justTouched()){
			progressbar.setScaleY(progressbar.getScaleY() + 15/100f);
		}
	}

}
