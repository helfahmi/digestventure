package com.ktat.dventure.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.ktat.dventure.main.DVenture;

/**
 * The base class for all game screens.
 */
public abstract class AbstractScreen implements Screen {
    // the fixed viewport dimensions (ratio: 1.6)
    /*public static final int GAME_VIEWPORT_WIDTH = 800, GAME_VIEWPORT_HEIGHT = 480;
    public static final int MENU_VIEWPORT_WIDTH = 800, MENU_VIEWPORT_HEIGHT = 480;*/

    protected final DVenture game;
    protected final Stage stage;
    
    OrthographicCamera camera;
    private BitmapFont font;
    private SpriteBatch batch;
    private Skin skin;
    private float r,g,b;
    private boolean isPaused = false;
    private Image label_pause;
    private ImageButton retry, quit;
    private Image fade;
    private Image kepala;
    private ImageButton pause, help;
    public Image hany, poison, straw;
    public float score;
    

    public AbstractScreen(DVenture game){
        this.game = game;
        /*int width = ( isGameScreen() ? GAME_VIEWPORT_WIDTH : MENU_VIEWPORT_WIDTH );
        int height = ( isGameScreen() ? GAME_VIEWPORT_HEIGHT : MENU_VIEWPORT_HEIGHT );*/
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 480);
        stage = new Stage(800, 480, true );
        stage.setCamera(camera);
        r = g = b = 0f;
    }

    protected String getName(){
        return getClass().getSimpleName();
    }

    protected boolean isGameScreen(){
        return false;
    }

    // Lazily loaded collaborators
    
    public void setBackgroundColor(float r, float g, float b){
    	this.r = r;
    	this.g = g;
    	this.b = b;
    }
    
    public void setBackgroundColor(Color c){
    	this.r = c.r;
    	this.g = c.g;
    	this.b = c.b;
    }
    
    public void fadeIn(){
    	for(Actor actor: stage.getActors()){
    		actor.addAction(Actions.sequence(Actions.alpha(0), Actions.fadeIn(1f)));
    	}
    }
    
    public void fadeOutAndChangeTo(final AbstractScreen newScreen) {
		Runnable onFinish = new Runnable(){
			@Override
			public void run() {
				game.setScreen(newScreen);
			}
		};
		
		for(Actor actor: stage.getActors()){
			actor.addAction(Actions.fadeOut(1f));
		}
		stage.addAction(Actions.sequence(Actions.delay(1f), Actions.run(onFinish)));
	}
    
    public BitmapFont getFont(){
        if( font == null ) {
            font = new BitmapFont();
        }
        return font;
    }

    public SpriteBatch getBatch(){
        if( batch == null ) {
            batch = new SpriteBatch();
        }
        return batch;
    }

    public TextureAtlas getAtlas(){
        /*if(atlas == null) {
            atlas = new TextureAtlas(Gdx.files.internal("atlas/DVmain.atlas"));
        }
        return atlas;*/
    	return game.getMainAtlas();
    }

    protected Skin getSkin(){
        if( skin == null ) {
        	skin = new Skin(Gdx.files.internal("skins/menuskin.json"));
            FileHandle skinFile = Gdx.files.internal( "skin/uiskin.json" );
            FileHandle textureFile = Gdx.files.internal( "skin/uiskin.png" );
            //skin = new Skin(skinFile, new TextureAtlas(textureFile));
        }
        return skin;
    }

    // Screen implementation

    @Override
    public void show()
    {
        Gdx.app.log(DVenture.LOG, "Showing screen: " + getName() );

        // set the stage as the input processor
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void resize(int width, int height){
        Gdx.app.log( DVenture.LOG, "Resizing screen: " + getName() + " to: " + width + " x " + height );
        stage.setViewport(800, 480, true);
        camera.translate(-stage.getGutterWidth(), -stage.getGutterHeight(), 0);
    }

    @Override
    public void render(float delta){
        // (1) process the game logic

        // update the actors
    	if (!isPaused) {
    		stage.act( delta );
    	}
    	camera.update();
        
        // (2) draw the result

        // clear the screen with the given RGB color (black)
        Gdx.gl.glClearColor(r, g, b, 1f);
        Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT );

        // draw the actors
        stage.getSpriteBatch().setProjectionMatrix(camera.combined);
        stage.draw();
    }

    @Override
    public void hide(){
        Gdx.app.log(DVenture.LOG, "Hiding screen: " + getName());

        // dispose the screen when leaving the screen;
        // note that the dipose() method is not called automatically by the
        // framework, so we must figure out when it's appropriate to call it
        dispose();
    }
    
    @Override
    public void pause(){
        Gdx.app.log(DVenture.LOG, "Pausing screen: " + getName());
    }

    @Override
    public void resume(){
        Gdx.app.log(DVenture.LOG, "Resuming screen: " + getName());
    }

    @Override
    public void dispose(){
        Gdx.app.log(DVenture.LOG, "Disposing screen: " + getName());

        // as the collaborators are lazily loaded, they may be null
        if(font != null) font.dispose();
        if(batch != null) batch.dispose();
        if(skin != null) skin.dispose();
        //if(atlas != null) atlas.dispose();
    }
    
    public void setPause(boolean p) {
    	isPaused = p;
    }
    
    public boolean getPause() {
    	return isPaused;
    }
    
    public void loadPauseScreen() {
    	label_pause = new Image(getAtlas().findRegion("pause_title"));
        label_pause.setX((800 - label_pause.getWidth())/2);
        label_pause.setY(480 - label_pause.getHeight() - 180);
        fade = new Image(getAtlas().findRegion("fade"));
        retry = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("resume_touched")), new TextureRegionDrawable(getAtlas().findRegion("resume_untouched")));
        quit =  new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("quit_touched")), new TextureRegionDrawable(getAtlas().findRegion("quit_untouched")));
        label_pause.setVisible(isPaused);
        retry.setVisible(isPaused);
        quit.setVisible(isPaused);
        fade.setVisible(isPaused);
        
        retry.setX(15);
        retry.setY(15);
        quit.setX(800 - quit.getWidth() - 15);
        quit.setY(15);
        
		retry.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.playButtonSound();
				setPause(false);
			}
		});
		quit.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.playButtonSound();
				isPaused = false;
				if (game.getMode().equals("story")){
					game.pauseStory();
				} else {
					game.pauseArcade();
				}
				fadeOutAndChangeTo(new MenuScreen(game));
			}
		});
        
        stage.addActor(fade);
        stage.addActor(label_pause);
        stage.addActor(retry);
        stage.addActor(quit);
    }
    
    public void updatePause() {
    	label_pause.setVisible(isPaused);
        retry.setVisible(isPaused);
        quit.setVisible(isPaused);
        fade.setVisible(isPaused);
    }
    
    public void loadKepala() {
    	 kepala = new Image(getAtlas().findRegion("head"));
    	 kepala.setPosition(26, 480 - kepala.getHeight() - 72);
    	 stage.addActor(kepala);
    }
    
    public void loadHelpPauseButton() {
    	pause = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("pause_untouched")), new TextureRegionDrawable(getAtlas().findRegion("pause_touched")));
		help = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("help_untouched")), new TextureRegionDrawable(getAtlas().findRegion("help_touched")));
		help.setPosition(78, 480 - help.getHeight() - 4);
		pause.setPosition(8, 480 - help.getHeight() - 4);
		pause.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				setPause(true);
			}
		});
		
		stage.addActor(help);
		stage.addActor(pause);
		
    }
    
    public void startKepalaAnimation(String status, final AbstractScreen nextScreen) {
    	Runnable r = new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				fadeOutAndChangeTo(nextScreen);
			}
		};
		
    	if (status == "menang") {
    		kepala.addAction(Actions.sequence(Actions.moveTo(46, 290, 2f), Actions.run(r)));
    	} else if (status == "kalah") {
    		kepala.addAction(Actions.sequence(Actions.moveTo(0, 290, 2f), Actions.run(r)));
    	}
    }
    
    public void loadInfus() {
    	hany = new Image(getAtlas().findRegion("honey"));
		poison = new Image(getAtlas().findRegion("poison"));
		straw = new Image(getAtlas().findRegion("straw"));
		stage.addActor(straw);
		stage.addActor(hany);
		stage.addActor(poison);
		
    }
}
