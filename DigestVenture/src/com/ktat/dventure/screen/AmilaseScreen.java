package com.ktat.dventure.screen;

import java.util.ArrayList;
import java.util.Random;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.ktat.dventure.main.DVenture;
import com.ktat.dventure.objects.DialogManager;
import com.ktat.dventure.objects.DialogManager.Direction;
import com.ktat.dventure.objects.ImageAnimation;

public class AmilaseScreen extends AbstractScreen {

	private Image sidebarR, sidebarL, bg;
	private ImageAnimation brain;
	//stage spesific
	private Image glandL, glandR, spitL, spitR;
	private ImageButton buttonL, buttonR;
	private ArrayList<Image> bubbles;
	private int numBubbles = 0;
	private DialogManager dialog;
	
	private float speed = 3;
	private float progress = 0;
	private float time = 0;
	private String status = "tutorial";
	private final int POINT_SIZE = 20; //artinya 5x baru menang]
	private Image mulai;
	
	public AmilaseScreen(DVenture game) {
		super(game);
		sidebarR = new Image(getAtlas().findRegion("sidebar"));
		sidebarL = new Image(getAtlas().findRegion("sidebar"));
		bg = new Image(getAtlas().findRegion("bg_amilase"));
		brain = new ImageAnimation(getAtlas().findRegions("brainleft"), 0.050f);
		
		glandL = new Image(getAtlas().findRegion("salivagland_red"));
		glandR = new Image(getAtlas().findRegion("salivagland_green"));
		
		buttonL = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("button_red_unpressed")), new TextureRegionDrawable(getAtlas().findRegion("button_red_pressed")));
		buttonR = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("button_green_unpressed")), new TextureRegionDrawable(getAtlas().findRegion("button_green_pressed")));
		
		spitL = new Image(getAtlas().findRegion("spittlesleft"));
		spitR = new Image(getAtlas().findRegion("spittlesright"));
		
		mulai = new Image(getAtlas().findRegion("go"));		
		mulai.setPosition((800 - mulai.getWidth())/2, (480 - mulai.getHeight())/2);
		mulai.addAction(Actions.alpha(0f));
		
		//init bubble
		bubbles = new ArrayList<Image>();
		for (int i=0;i<3;i++){
			bubbles.add(new Image(getAtlas().findRegion("protein")));
			bubbles.get(i*3).setPosition(getPosition(), 250);
			bubbles.add(new Image(getAtlas().findRegion("fat")));
			bubbles.get(i*3+1).setPosition(getPosition(), 250);
			bubbles.add(new Image(getAtlas().findRegion("glucose")));
			bubbles.get(i*3+2).setPosition(getPosition(), 250);
		}
		
		sidebarR.setX(800 - sidebarR.getWidth());
		bg.setX(149);
		brain.setPosition(600, 328);
		
		glandL.setPosition(180, 0);
		glandR.setPosition(530, 0);
		
		spitL.setPosition(220, 20);
		spitR.setPosition(510, 20);
		
		buttonL.setPosition(675, 200);
		buttonR.setPosition(675, 100);
		
		
		//buttons
		buttonL.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				spitL.setPosition(220, 20);
				spitL.addAction(Actions.sequence(Actions.moveBy(80, 0, 0.05f), Actions.moveBy(-80, 0, 0.3f)));
				//cek bubble
				for (int i=0;i<numBubbles;i++){
					if ((bubbles.get(i).getY() < 40)&&(bubbles.get(i).getX() == 335)){
						bubbles.get(i).setVisible(false);
						if (status == "belom"){
							progress += POINT_SIZE;
							hany.setScaleY(progress/100f);
						}
					}
				}
			}
		});
		
		buttonR.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				spitR.setPosition(510, 20);
				spitR.addAction(Actions.sequence(Actions.moveBy(-80, 0, 0.05f), Actions.moveBy(80, 0, 0.3f)));
				//cek bubble
				for (int i=0;i<numBubbles;i++){
					if ((bubbles.get(i).getY() < 40)&&(bubbles.get(i).getX() == 415)){
						bubbles.get(i).setVisible(false);
						if (status == "belom"){
							progress += POINT_SIZE;
							hany.setScaleY(progress/100f);
						}
					}
				}
			}
		});
		
		
		
		stage.addActor(bg);
		stage.addActor(sidebarR);
		stage.addActor(sidebarL);
		stage.addActor(brain);
		stage.addActor(glandL);
		stage.addActor(glandR);
		stage.addActor(buttonL);
		stage.addActor(buttonR);
		stage.addActor(spitL);
		stage.addActor(spitR);
		stage.addActor(mulai);
		
		//tutrial
		dialog = new DialogManager(getAtlas().findRegion("dialogbox"), getAtlas().findRegion("pointer_left"),  getAtlas().findRegion("pointer_right"));
		dialog.addMsg("Hancurkan bola-bola makanan ini...", 150, 335, 225, 260, Direction.RIGHT);
		dialog.addMsg("Dengan mengaktifkan kelenjar amilase...", 150, 150, 397, 21, Direction.RIGHT);
		dialog.addMsg("Yang diaktifkan dengan tombol ini.", 150, 150, 561, 102, Direction.RIGHT);
		dialog.setOnFinishAction(new Runnable(){
			@Override
			public void run() {
				mulai.addAction(Actions.sequence(Actions.fadeIn(0.5f), Actions.delay(0.5f), Actions.run(new Runnable(){
					@Override
					public void run() {
						mulai.remove();
						status = "belom";
					}
				})));
			}
		});
		dialog.init();
		

		loadKepala();
		loadInfus();
		hany.setScaleY(progress/100f);
		poison.setScaleY(time/100f);
		for (int i=0;i<8;i++){
			stage.addActor(bubbles.get(i));
		}
		loadHelpPauseButton();
		stage.addActor(dialog);
		loadPauseScreen();
		
		//tutrial
		dialog = new DialogManager(getAtlas().findRegion("dialogbox"), getAtlas().findRegion("pointer_left"),  getAtlas().findRegion("pointer_right"));
		dialog.addMsg("Hancurkan bola-bola ini", 150, 250, 580, 221, Direction.RIGHT);
		dialog.addMsg("Dengan mengaktifkan kelenjar ini", 150, 150);
		dialog.addMsg("Yang diaktifkan dengan tombol ini", 150, 150);
		dialog.setOnFinishAction(new Runnable(){
			@Override
			public void run() {
				status = "belom";
			}
		});
		dialog.init();
		stage.addActor(dialog);
	}
	
	public int getPosition(){
		return new Random().nextInt(2)*80+335;
	}
	
	@Override
	public void render(float delta) {
		super.render(delta);
		super.updatePause();
		if (status == "menang") {
			System.gc();
			int score;
			int poisons = (int) (poison.getScaleY()*100f);
			score = poisons * 50;
			game.setArcadeScore(2, score);
			game.setLevelCompleted(3);
			startKepalaAnimation(status, new ScoreScreen(game, "AmilaseScreen"));
		} else if (status == "kalah") {
			System.gc();
			startKepalaAnimation(status, new GameOverScreen(game));
		}
		if (!getPause() && status == "belom") {
			//Infus
			if (progress >= 100) {
				progress = 100;
				status = "menang";
				//game.setScreen(new ModeScreen(game));
			}
			
			if (poison.getScaleY() >= 1) {
				status = "kalah";
			}
			if (poison.getScaleY() < 1) {
				time += speed;
				poison.setScaleY((float) (time*0.05/100f));
			}
			
			if (numBubbles<8){
				if (numBubbles > 0){
					if (bubbles.get(numBubbles-1).getY()<190){
						//stage.addActor(bubbles.get(numBubbles));
						numBubbles++;
					}
				} else {
					//kalo belum ada yang spawn
					//stage.addActor(bubbles.get(numBubbles));
					numBubbles++;
				}
			}
			
			//update bubbles
			for (int i=0;i<numBubbles;i++){
				bubbles.get(i).setY(bubbles.get(i).getY()-50*delta);
			}
			
			//cek apa udah sampe bawah
			for (int i=0;i<numBubbles;i++){
				if (bubbles.get(i).getY()<10){
					bubbles.get(i).setVisible(true);
					bubbles.get(i).setPosition(getPosition(), 250);
				}
			}
		}
	}
}
