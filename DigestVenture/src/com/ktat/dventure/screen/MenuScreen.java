package com.ktat.dventure.screen;

import com.badlogic.gdx.Audio;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.ktat.dventure.main.DVenture;
import com.ktat.dventure.objects.ImageAnimation;

public class MenuScreen extends AbstractScreen{

	private ImageAnimation stomach, intestine, duodenum;
	private Image title, bg, cloud1, cloud2, cloud3, table, plate, plate2, plate3;
	private ImageButton play, conf, sound_on, sound_off, about, help, quit;
	private int border = 15;
	private boolean isConfigActive = false;
	public static boolean isMute = false;
	
	public MenuScreen(final DVenture game) {
		super(game);
		
		/* ImageAnimation
		 * Parameter 1:
		 * 		Ambil image(s) yg prefixnya "brain".
		 * Parameter 2:
		 * 		Waktu delay antar frame, dalam detik.	
		 * */
		
		//INIT
		play = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("play_untouched")), new TextureRegionDrawable(getAtlas().findRegion("play_touched")));
		conf = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("setting_untouched")), new TextureRegionDrawable(getAtlas().findRegion("setting_touched")));
		sound_on = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("sound_on")), new TextureRegionDrawable(getAtlas().findRegion("sound_on")));
		sound_off = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("sound_off")), new TextureRegionDrawable(getAtlas().findRegion("sound_off")));
		about = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("about_untouched")), new TextureRegionDrawable(getAtlas().findRegion("about_touched")));
		help = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("help_untouched")), new TextureRegionDrawable(getAtlas().findRegion("help_touched")));
		quit = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("quit_untouched")), new TextureRegionDrawable(getAtlas().findRegion("quit_touched")));
		
		plate = new Image(getAtlas().findRegion("plate"));
		plate2 = new Image(getAtlas().findRegion("plate"));
		plate3 = new Image(getAtlas().findRegion("plate"));
		table = new Image(getAtlas().findRegion("table"));
		bg = new Image(getAtlas().findRegion("background"));
		title = new Image(getAtlas().findRegion("title"));
		
		stomach = new ImageAnimation(getAtlas().findRegions("stomachright"), 0.080f);
		intestine = new ImageAnimation(getAtlas().findRegions("intestine"), 0.100f, Animation.LOOP_PINGPONG);
		duodenum = new ImageAnimation(getAtlas().findRegions("duodenumleft"), 0.150f);
		
		cloud1 = new Image(getAtlas().findRegion("cloud1"));
		cloud2 = new Image(getAtlas().findRegion("cloud2"));
		cloud3 = new Image(getAtlas().findRegion("cloud3"));
		
		//BUTTON PLACING
		play.setX((800 - play.getWidth())/2);
		play.setY(play.getHeight());
		conf.setX(border);
		conf.setY(480 - conf.getHeight() - border);
		quit.setX(800 - quit.getWidth() - border);
		quit.setY(480 - quit.getHeight() - border);
		
		sound_on.setX(126);
		sound_on.setY(480 - help.getHeight()- 15);
		sound_off.setX(126);
		sound_off.setY(480 - help.getHeight()- 15);
		about.setX(105);
		about.setY(480 - help.getHeight() - 96);
		help.setX(border);
		help.setY(480 - help.getHeight() - 125);
		
		sound_on.setVisible(!isMute);
		sound_off.setVisible(isMute);
		about.setVisible(isConfigActive);
		help.setVisible(isConfigActive);
		
		//ADD BUTTON LISTENER
		play.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				//play.removeListener(this);
				fadeOutAndChangeTo(new ModeScreen(game));
				game.playButtonSound();
				//game.setScreen(new ModeScreen(game));
			}
		});
		conf.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.playButtonSound();
				if (isConfigActive == false)
					isConfigActive = true;
				else
					isConfigActive = false;	
			}
		});
		sound_on.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.playButtonSound();
				isMute = true;
				game.setMute(true);
				game.menuMusic.pause();
			}
		});
		sound_off.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.playButtonSound();
				isMute = false;
				game.setMute(false);
				game.menuMusic.play();
			}
		});
		about.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				fadeOutAndChangeTo(new AboutScreen(game));
				game.playButtonSound();
			}
		});
		help.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				fadeOutAndChangeTo(new HelpScreen(game));
				game.playButtonSound();
			}
		});
		quit.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.playButtonSound();
				game.setScreen(new QuitScreen(game, "MenuScreen"));
			}
		});
		
		//STATIC ELEMENT PLACING
		title.setX((800 - title.getWidth()) / 2);
		title.setY(480 - title.getHeight() - 50);
		
		plate.setPosition(305, 100);
		plate2.setPosition(110, 31);
		plate3.setPosition(515, 23);
		
		//ANIMATION PLACING
		stomach.setX((800 - stomach.getWidth()) / 2 - 40);
		stomach.setY(480 - stomach.getHeight() - 20);
		
//		colon.setY(border + 15);
//		colon.setX(-cloud1.getWidth() - 20);
		intestine.setPosition(128, 42);
		
		duodenum.setPosition(540, 38);
		
//		duodenum.setX(800 + 25);
//		duodenum.setY(border + 25);
		
		cloud1.setX(-cloud1.getWidth());
		cloud1.setY(border);
		cloud3.setX(800);
		cloud3.setY(border);
		
		stage.addActor(bg);
		stage.addActor(table);
		stage.addActor(plate);
		stage.addActor(plate2);
		stage.addActor(plate3);
		//stage.addActor(cloud1);
		//stage.addActor(cloud3);
		stage.addActor(duodenum);
		stage.addActor(intestine);
		stage.addActor(quit);
		stage.addActor(conf);
		stage.addActor(sound_on);
		stage.addActor(sound_off);
		stage.addActor(about);
		stage.addActor(help);
		
		//stomach.addAction(Actions.fadeOut(10.f));
		for(Actor actor: stage.getActors()){
			actor.addAction(Actions.sequence(Actions.alpha(0), Actions.fadeIn(1f)));
		}
		
		stage.addActor(stomach);
		stage.addActor(title);
		stage.addActor(play);
		
		//play music
		if (!game.getMute()){
			game.menuMusic.play();
		}
		
		stomach.addAction(Actions.sequence(Actions.alpha(0), Actions.delay(3.2f), Actions.fadeIn(1f)));
		title.addAction(Actions.sequence(Actions.alpha(0), Actions.delay(1f), Actions.fadeIn(1f), Actions.scaleTo(1.1f, 1.1f, 0.5f), Actions.scaleTo(1f, 1f, 0.5f)));
		play.addAction(Actions.sequence(Actions.alpha(0), Actions.delay(1f), Actions.fadeIn(2f)));
	}

	@Override
    public void render(float delta){
      super.render(delta);
      sound_on.setVisible(isConfigActive && !isMute);
      sound_off.setVisible(isConfigActive && isMute);
      about.setVisible(isConfigActive);
      help.setVisible(isConfigActive);
      
      
      //Maju Mundur
//      if (cloud1.getX() + cloud1.getWidth() >= 400) {
//    	  isMundur = true;
//      }
//      else if (cloud1.getX() <= 0) {
//    	 isMundur = false;
//      }
//      
//      if (!isMundur) {
//    	  cloud1.setX(cloud1.getX()+70*delta);
//    	  colon.setX(colon.getX()+70*delta);
//    	  cloud3.setX(cloud3.getX()-70*delta);
//    	  duodenum.setX(duodenum.getX()-70*delta);
//      } else {
//    	  cloud1.setX(cloud1.getX()-70*delta);
//    	  colon.setX(colon.getX()-70*delta);
//    	  cloud3.setX(cloud3.getX()+70*delta);
//    	  duodenum.setX(duodenum.getX()+70*delta);
//      }
    }

}
