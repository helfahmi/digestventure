package com.ktat.dventure.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.ktat.dventure.main.DVenture;
import com.ktat.dventure.objects.ImageAnimation;

public class QuitScreen extends AbstractScreen{

	private Image text, bg;
	private ImageButton yes, no;
	private int border = 15;
	private String PreviousScreen;
	
	public QuitScreen(final DVenture game, final String PreviousScreen) {
		super(game);
		this.PreviousScreen = PreviousScreen;
		
		//INIT
		yes = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("yes_untouched")), new TextureRegionDrawable(getAtlas().findRegion("yes_touched")));
		no = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("no_untouched")), new TextureRegionDrawable(getAtlas().findRegion("no_touched")));
		bg = new Image(getAtlas().findRegion("background"));
		text = new Image(getAtlas().findRegion("exit_conf"));
		
		//BUTTON PLACING
		float space = ((800 - yes.getWidth() - no.getWidth()) / 3);
		yes.setX(space);
		yes.setY(200);
		no.setX(space+ yes.getWidth() + space);
		no.setY(200);
		
		yes.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.playButtonSound();
				Gdx.app.exit();
			}
		});
		no.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.playButtonSound();
				if (PreviousScreen == "MenuScreen")
					game.setScreen(new MenuScreen(game));
			}
		});
		//STATIC ELEMENT PLACING
		space = ((800 - text.getWidth()) / 2);
		text.setX(space);
		text.setY(350);
		
		//ANIMATION PLACING
		
		
		stage.addActor(bg);
		stage.addActor(text);
		stage.addActor(yes);
		stage.addActor(no);
	}

}
