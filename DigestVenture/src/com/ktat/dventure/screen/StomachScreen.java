package com.ktat.dventure.screen;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.ktat.dventure.main.DVenture;
import com.ktat.dventure.objects.Bubble;
import com.ktat.dventure.objects.DialogManager;
import com.ktat.dventure.objects.FunFact;
import com.ktat.dventure.objects.ImageAnimation;

public class StomachScreen extends AbstractScreen{
	private Image sidebarR, sidebarL, bg;
	private ImageAnimation brain;
	private float curProgress = 0f;
	private DialogManager dManager;
	//Stomach Attribute spesific
	private Image stomach;
	private ArrayList<Bubble> bubbles;
	private ImageAnimation acid;
	private int numBubble = 4;
	private float counter;
	private float time = 0f;
	private int widthRange = 170;
	private DialogManager dialog;
	private final int POINT_SIZE = 10;
	private float speed = 3;
	private String status = "tutorial"; //belom, menang, kalah
	private Image mulai;

	public StomachScreen(DVenture game) {
		super(game);
		//Animation & Buttons
		acid = new ImageAnimation(getAtlas().findRegions("acid"), 0.20f);
		sidebarR = new Image(getAtlas().findRegion("sidebar"));
		sidebarL = new Image(getAtlas().findRegion("sidebar"));
		bg = new Image(getAtlas().findRegion("bg_game"));
		bg.setX(150);
		brain = new ImageAnimation(getAtlas().findRegions("brainleft"), 0.050f);
		stomach = new Image(getAtlas().findRegion("stomachstage"));
		mulai = new Image(getAtlas().findRegion("go"));		
		mulai.setPosition((800 - mulai.getWidth())/2, (480 - mulai.getHeight())/2);
		mulai.addAction(Actions.alpha(0f));

		//Bubbles
		bubbles = new ArrayList<Bubble>();
		for (int i=0;i<numBubble;i++){
			bubbles.add(new Bubble("protein",this));
			bubbles.add(new Bubble("fat", this));
			bubbles.add(new Bubble("glucose", this));
		}
		setBubbles();
		
		//Counter
		counter = 0;
		
		int stomachX = 75;
		int stomachY = 0;
		
		stomach.setX(stomachX);
		stomach.setY(stomachY);

		acid.setX(stomachX+120);
		acid.setY(stomachY+10);
		
		sidebarR.setX(800 - sidebarR.getWidth());
		brain.setPosition(600, 328);
		
		curProgress = 0;
		
		stage.addActor(bg);
		stage.addActor(stomach);
		stage.addActor(mulai);
		
		//Set bubble on stage
		for (int i=0;i<numBubble*3;i++){
			stage.addActor(bubbles.get(i));
			final int j = i;
			bubbles.get(i).addListener(new ClickListener(){
				public void clicked(InputEvent event, float x, float y){
					if ((bubbles.get(j).getType()=="protein")&&(status == "belom")){
						bubbles.get(j).setVisible(false);
						resetBubble(j);
						curProgress += POINT_SIZE;
						hany.setScaleY(curProgress/100f);
						if (curProgress > 100){
							curProgress = 100;
						}
					} else {
						bubbles.get(j).setBackground(new TextureRegionDrawable(getAtlas().findRegion("protein")));
					}
				}
			});
		}
		
		//help dialog
		dialog = new DialogManager(getAtlas().findRegion("dialogbox"), getAtlas().findRegion("pointer_left"),  getAtlas().findRegion("pointer_right"));
		dialog.addMsg("Tekan bola-bola biru", 150, 250);
		dialog.addMsg("Jangan sampai bola biru jatuh ke asam", 150, 150);
		dialog.setOnFinishAction(new Runnable(){
			@Override
			public void run() {
				mulai.addAction(Actions.sequence(Actions.fadeIn(0.5f), Actions.delay(0.5f), Actions.run(new Runnable(){
					@Override
					public void run() {
						mulai.remove();
						status = "belom";
					}
				})));
			}
		});
		dialog.init();
				
		stage.addActor(acid);
		stage.addActor(sidebarR);
		stage.addActor(sidebarL);
		stage.addActor(brain);
		stage.addActor(dialog);
		loadKepala();
		loadInfus();
		hany.setScaleY(curProgress/100f);
		poison.setScaleY(time/100f);
		loadHelpPauseButton();
		loadPauseScreen();
	}
	
	public void setBubbles(){
		int x;
		for (int i=0;i<numBubble*3;i++){
			x = new Random().nextInt(widthRange);
			bubbles.get(i).setX(x+350);
			bubbles.get(i).setY(370);
		}
	}
	
	public void resetBubble(int i){
		bubbles.get(i).randomSpeed();
		bubbles.get(i).setX((float)new Random().nextInt(widthRange)+350);
		bubbles.get(i).setVisible(true);
		bubbles.get(i).setY(370);
	}
	
	@Override
	public void render(float delta){
		super.render(delta);
		super.updatePause();
		if (status == "menang") {
			System.gc();
			int score;
			int poisons = (int) (poison.getScaleY()*100f);
			score = poisons * 50;
			game.setArcadeScore(4, score);
			game.setLevelCompleted(5);
			startKepalaAnimation(status, new ScoreScreen(game, "StomachScreen"));
		} else if (status == "kalah") {
			System.gc();
			startKepalaAnimation(status, new GameOverScreen(game));
		}
		if (!getPause() && status == "belom") {			
			//Infus
			if (curProgress >= 100) {
				curProgress = 100;
				status = "menang";
				//game.setScreen(new ModeScreen(game));
			} else {
				hany.setScaleY(curProgress/100f);
			}
			
			if (poison.getScaleY() >= 1) {
				status = "kalah";
			}
			if (poison.getScaleY() < 1) {
				time += speed;
				poison.setScaleY((float) (time*0.05/100f));
			}

			for (int i=0;i<numBubble*3;i++){
				bubbles.get(i).setY(bubbles.get(i).getY()-bubbles.get(i).getSpeed()*delta);
				if (bubbles.get(i).getY()<120){
					if (bubbles.get(i).getType().equals("protein")&&bubbles.get(i).isVisible()){
						curProgress -= 10;
						if (curProgress < 0){
							curProgress = 0;
						}
					}
					resetBubble(i);
				}
			}
		}
	}
}
