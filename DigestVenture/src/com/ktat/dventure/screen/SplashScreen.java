package com.ktat.dventure.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.ktat.dventure.main.DVenture;
import com.ktat.dventure.objects.LidahRegion;

public class SplashScreen extends AbstractScreen{

	private Image logo;
	private AssetManager manager;
	private float waitTime = 1f;
	private Label studioName;
	public SplashScreen(final DVenture game, AssetManager manager) {
		super(game);
		
		this.manager = manager;
		logo = new Image(getAtlas().findRegion("logo"));
		Skin skin = getSkin();
		studioName = new Label("KTat Studio", getSkin());
		studioName.setFontScale(0.7f);
		studioName.setPosition(245, 15);
		logo.setX((800 - logo.getWidth())/2);
		logo.setY((480 - logo.getHeight())/2 + 45);
		Color bgColor = Color.valueOf("f0f9b6");
		setBackgroundColor(Color.valueOf("f0f9b6"));
		//logo.setColor(logo.getColor().r, logo.getColor().g, logo.getColor().b, 0f);
		logo.addAction(Actions.sequence(Actions.alpha(0), Actions.fadeIn(2f)));
		studioName.addAction(Actions.sequence(Actions.alpha(0), Actions.fadeIn(2f)));
		stage.addActor(logo);
		stage.addActor(studioName);
		manager.load("atlas/DVmain.atlas", TextureAtlas.class);
	}
	
	Runnable onLoadFinish = new Runnable() {
	    @Override
	    public void run() {
	    	game.setMainAtlas(manager.get("atlas/DVmain.atlas", TextureAtlas.class));
			game.setScreen(new MenuScreen(game));
	    }
	};
		
	@Override
	public void render(float delta){
		super.render(delta);
		
		if(manager.update() && waitTime < 0){
			logo.addAction(Actions.sequence(Actions.fadeOut(2f), Actions.run(onLoadFinish)));
			studioName.addAction(Actions.fadeOut(2f));
		} else {
			waitTime -= delta;
			Gdx.app.log(DVenture.LOG, "waitTime: " + waitTime);
		}
		
		//float progress = manager.getProgress();
	}
	
	@Override
	public TextureAtlas getAtlas(){
		return game.getSplashAtlas();
	}
}
