package com.ktat.dventure.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.ktat.dventure.main.DVenture;

public class FinishedScreenDark extends AbstractScreen {
	private Image bgDark, fart;
	private Image siluet[];

	public FinishedScreenDark(final DVenture game) {
		super(game);
		siluet = new Image[8];
		siluet[0] = new Image(getAtlas().findRegion("brain_dark"));//brain
		siluet[0].setPosition(610, 350);
		siluet[1] = new Image(getAtlas().findRegion("tongueleft_dark"));//tongue left
		siluet[1].setPosition(320, 20);
		siluet[2] = new Image(getAtlas().findRegion("tongueright_dark"));//tongue right
		siluet[2].setPosition(270, 30);
		siluet[3] = new Image(getAtlas().findRegion("esophagus_dark"));//esofagus
		siluet[3].setPosition(650, 20);
		siluet[4] = new Image(getAtlas().findRegion("stomach_dark"));//stomach
		siluet[4].setPosition(350, 30);
		siluet[5] = new Image(getAtlas().findRegion("duodenum_dark"));//duodenum
		siluet[5].setPosition(50, 20);
		siluet[6] = new Image(getAtlas().findRegion("intestine_dark"));//intestine
		siluet[6].setPosition(530, 50);
		siluet[7] = new Image(getAtlas().findRegion("colon_dark"));//colon
		siluet[7].setPosition(30, 70);
		
		bgDark = new Image(getAtlas().findRegion("bg_dark"));
		
		fart = new Image(getAtlas().findRegion("fart"));
		fart.setPosition(210, 280);
		fart.setVisible(false);
		
		stage.addActor(bgDark);
		for (int i=0;i<8;i++){
			stage.addActor(siluet[i]);
		}
		
		Runnable runnable = new Runnable() {
			
			@Override
			public void run() {
				fadeOutAndChangeTo(new FinishedScreenLight(game));				
			}
		};
		//fart animation
		stage.addActor(fart);
		fart.addAction(Actions.sequence(Actions.delay(1.5f), Actions.visible(true), Actions.moveBy(-40, 20,1f), Actions.fadeOut(0.5f), Actions.visible(false), Actions.delay(1f), Actions.run(runnable)));
	}
}
