package com.ktat.dventure.screen;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.ktat.dventure.main.DVenture;

public class HelpScreen extends AbstractScreen{
	private Image bg;
	private ImageButton back;
	
	public HelpScreen(final DVenture game) {
		super(game);
		bg = new Image(getAtlas().findRegion("help"));
		back = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("back_untouched")), new TextureRegionDrawable(getAtlas().findRegion("back_touched")));
		back.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				fadeOutAndChangeTo(new MenuScreen(game));
			}
		});
		back.setX(15);
		back.setY(480 - back.getHeight() - 15);
		
		stage.addActor(bg);
		stage.addActor(back);
	}
	
	@Override
    public void render(float delta){
      super.render(delta);
	}
}
