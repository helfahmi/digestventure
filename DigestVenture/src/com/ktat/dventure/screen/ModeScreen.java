package com.ktat.dventure.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.ktat.dventure.main.DVenture;
import com.ktat.dventure.objects.ImageAnimation;

public class ModeScreen extends AbstractScreen{

	//private ImageAnimation tongue,stomach;
	private Image bg, cloud2, table, plate, burger;
	private ImageButton story, arcade, back;
	private int border = 15;
	private boolean isMundur = false;
	
	
	public ModeScreen(final DVenture game) {
		super(game);
		
		/* ImageAnimation
		 * Parameter 1:
		 * 		Ambil image(s) yg prefixnya "brain".
		 * Parameter 2:
		 * 		Waktu delay antar frame, dalam detik.	
		 * */
		
		//INIT
		story = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("story_untouched")), new TextureRegionDrawable(getAtlas().findRegion("story_touched")));
		arcade = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("arcade_untouched")), new TextureRegionDrawable(getAtlas().findRegion("arcade_touched")));
		back = new ImageButton(new TextureRegionDrawable(getAtlas().findRegion("back_untouched")), new TextureRegionDrawable(getAtlas().findRegion("back_touched")));
		
		bg = new Image(getAtlas().findRegion("background"));
//		tongue = new ImageAnimation(getAtlas().findRegions("tongueleft"), 0.090f);
//		stomach = new ImageAnimation(getAtlas().findRegions("stomachright"), 0.090f);
		cloud2 = new Image(getAtlas().findRegion("cloud2"));
		table = new Image(getAtlas().findRegion("table"));
		plate = new Image(getAtlas().findRegion("plate"));
		burger = new Image(getAtlas().findRegion("hamburger"));
		
		//BUTTON PLACING
		float space = (800 - arcade.getWidth()) / 2;
		story.setX(space);
		story.setY(425 - story.getHeight());
		arcade.setX(space);
		arcade.setY(150);
		back.setX(border);
		back.setY(480 - back.getHeight() - border);

		//ADD BUTTON LISTENER
		story.addListener(new ClickListener() {
			private boolean click = false;
			
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.playButtonSound();
				story.removeListener(this);
				//setmode story
				game.pauseMenu();
				game.playStory();
				game.setMode("story");
				fadeOutToStory();
				//fadeOutToStory();
			}
		});
		arcade.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.playButtonSound();
				//setmode arcade
				game.setMode("arcade");
				game.setScreen(new MapScreen(game));
			}
		});
		back.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.playButtonSound();
				fadeOutAndChangeTo(new MenuScreen(game));
			}
		});
		
		//STATIC ELEMENT PLACING
		
		
		//ANIMATION PLACING
		plate.setPosition((800 - plate.getWidth())/2, 77);
		burger.setPosition(333, 90);
		cloud2.setX(-cloud2.getWidth());
		cloud2.setY(border);
//		tongue.setX(-cloud2.getWidth() + 60);
//		tongue.setY(border + 40);
//		stomach.setX(-stomach.getWidth() - cloud2.getWidth() + 20);
//		stomach.setY(border + 10);
		
		
		stage.addActor(bg);
		stage.addActor(table);
		stage.addActor(plate);
		stage.addActor(burger);
//		stage.addActor(cloud2);
//		stage.addActor(stomach);
//		stage.addActor(tongue);
		stage.addActor(story);
		stage.addActor(arcade);
		stage.addActor(back);
		
		fadeIn();
	}
	
	public void fadeOutToStory(){
		Runnable onFinish = new Runnable(){
			@Override
			public void run() {
				game.setScreen(new StoryScreen(game));
			}
		};
		
		story.addAction(Actions.fadeOut(1f));
		arcade.addAction(Actions.fadeOut(1f));
		back.addAction(Actions.sequence(Actions.fadeOut(1f), Actions.run(onFinish)));
	}
	
	@Override
    public void render(float delta){
		super.render(delta);
//		if (stomach.getX() >= 800) {
//    	  	cloud2.setX(-cloud2.getWidth());
//	  		tongue.setX(-cloud2.getWidth() + 60);
//	  		stomach.setX(-stomach.getWidth() - cloud2.getWidth() + 20);
//		} else {
//    	  cloud2.setX(cloud2.getX()+70*delta);
//    	  tongue.setX(tongue.getX()+70*delta);
//    	  stomach.setX(stomach.getX()+70*delta);
//		} 
    }

}
