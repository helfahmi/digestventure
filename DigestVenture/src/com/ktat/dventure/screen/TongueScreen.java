package com.ktat.dventure.screen;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.ktat.dventure.main.DVenture;
import com.ktat.dventure.objects.ClickableFood;
import com.ktat.dventure.objects.DialogManager;
import com.ktat.dventure.objects.ImageAnimation;
import com.ktat.dventure.objects.LidahRegion;
import com.ktat.dventure.objects.DialogManager.Direction;

public class TongueScreen extends AbstractScreen{
	private Image tongue, bg, sidebarL, sidebarR, head;
	private Image[] plate;
	private String[] plateTemplate;
	private LidahRegion bitter, sweet, sourL, sourR, saltL, saltR;
	private ImageAnimation brain;
	private String selectedFoodColor;
	private LidahRegion[] regions;
	private ClickableFood[] foods;
	private float speed = 3;
	private float progress = 0f;
	private float time = 0f;
	private Image mulai;
	private final float POINT_SIZE = 20;
	
	private String status = "tutorial";
	private DialogManager dmanager;
	private float addedPercent;
	private Random random;
	private Label skip;
	
	public TongueScreen(final DVenture game) {
		super(game);
		
		random = new Random();
		skip = new Label("Skip Tutorial", getSkin());
		selectedFoodColor = "none";
		regions = new LidahRegion[6];
		foods = new ClickableFood[4];
		plate = new Image[4];
		plateTemplate = new String[4]; 
		plateTemplate[0] = "sour";
		plateTemplate[1] = "sweet";
		plateTemplate[2] = "bitter";
		plateTemplate[3] = "salt";
		
		for(int i=0;i<4;i++){
			String usedTemplate = plateTemplate[random.nextInt(4)];
			plate[i] = new Image(getAtlas().findRegion("plate_small_" + usedTemplate));
			if(usedTemplate.equals("salt")){
				foods[i] = new ClickableFood(this, getAtlas().findRegion("friedchicken"), getAtlas().findRegion("fork"));
				foods[i].setName(usedTemplate);
			} else if(usedTemplate.equals("sweet")){
				foods[i] = new ClickableFood(this, getAtlas().findRegion("banana"), getAtlas().findRegion("fork"));
				foods[i].setName(usedTemplate);
			} else if(usedTemplate.equals("bitter")){
				foods[i] = new ClickableFood(this, getAtlas().findRegion("darkchocolate"), getAtlas().findRegion("fork"));
				foods[i].setName(usedTemplate);
			} else if(usedTemplate.equals("sour")){
				foods[i] = new ClickableFood(this, getAtlas().findRegion("strawberry"), getAtlas().findRegion("fork"));
				foods[i].setName(usedTemplate);
			}
		}
		
		mulai = new Image(getAtlas().findRegion("go"));		
		mulai.setPosition((800 - mulai.getWidth())/2, (480 - mulai.getHeight() - 50) );
		//mulai.setColor(mulai.getColor().r, mulai.getColor().g, mulai.getColor().b, 0);
		mulai.setVisible(false);
		
		sidebarL = new Image(getAtlas().findRegion("sidebar"));
		sidebarR = new Image(getAtlas().findRegion("sidebar"));
		brain = new ImageAnimation(getAtlas().findRegions("brainleft"), 0.050f);
		bg = new Image(getAtlas().findRegion("bg_game"));
		tongue = new Image(getAtlas().findRegion("bg_tongue"));
		/* Foods */
		
		
		bitter = new LidahRegion(getAtlas().findRegion("bitter_up"), getAtlas().findRegion("bitter_down"));
		sweet = new LidahRegion(getAtlas().findRegion("sweet_up"), getAtlas().findRegion("sweet_down"));
		
		sourL = new LidahRegion(getAtlas().findRegion("sourleft_up"), getAtlas().findRegion("sourleft_down"));
		sourR = new LidahRegion(getAtlas().findRegion("sourright_up"), getAtlas().findRegion("sourright_down"));
		
		saltL = new LidahRegion(getAtlas().findRegion("saltleft_up"), getAtlas().findRegion("saltleft_down"));
		saltR = new LidahRegion(getAtlas().findRegion("saltright_up"), getAtlas().findRegion("saltright_down"));
		
		sweet.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				
				if(getSelectedFoodColor().equals("sweet") && status == "belom"){
					removeForks();
					progress += POINT_SIZE;
				}
			}
		});
		
		bitter.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				
				if(getSelectedFoodColor().equals("bitter") && status == "belom"){
					removeForks();
					progress += POINT_SIZE;
				}
			}
		});
		
		sourR.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				
				if(getSelectedFoodColor().equals("sour") && status == "belom"){
					removeForks();
					progress += POINT_SIZE;
				}
			}
		});
		
		sourL.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				
				if(getSelectedFoodColor().equals("sour") && status == "belom"){
					removeForks();
					progress += POINT_SIZE;
				}
			}
		});
		
		
		saltR.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				
				if(getSelectedFoodColor().equals("salt") && status == "belom"){
					removeForks();
					progress += POINT_SIZE;
				}
			}
		});
		
		saltL.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				
				if(getSelectedFoodColor().equals("salt") && status == "belom"){
					removeForks();
					progress += POINT_SIZE;
				}
			}
		});
		regions[0] = bitter;
		regions[1] = sweet;
		regions[2] = sourL;
		regions[3] = sourR;
		regions[4] = saltL;
		regions[5] = saltR;
		
		//foods[0].setPosition(0, 0);
		
		
		int tongueX = 150;
		int tongueY = 0;
		
		int foodX = 30;
		int foodY = 10;
		for(int i=0;i<4;i++){
			plate[i].setPosition(805, 275);
			foods[i].setPosition(805 + foodX, 275 + foodY);
		}
		
		for(int i=0;i<4;i++){
			plate[i].addAction(Actions.sequence(Actions.delay(i*2.5f), Actions.forever(Actions.sequence(Actions.moveTo(659, 275, 1f), Actions.moveTo(659, -50, 9f), Actions.moveTo(805, 275)))));
			foods[i].addAction(Actions.sequence(Actions.delay(i*2.5f), Actions.forever(Actions.sequence(Actions.moveTo(659 + foodX, 275 + foodY, 1f), Actions.moveTo(659 + foodX, -50 + foodY, 9f), Actions.moveTo(805 + foodX, 275 + foodY)))));
		}
			
		
		bg.setPosition(150, 0);
		tongue.setPosition(tongueX, tongueY);
		bitter.setPosition(tongueX + 85, tongueY + 329);
		sweet.setPosition(tongueX + 143, tongueY + 48);
		
		sourL.setPosition(tongueX + 80, tongueY + 225);
		sourR.setPosition(tongueX + 313, tongueY + 225);
		
		saltL.setPosition(tongueX + 85, tongueY + 120);
		saltR.setPosition(tongueX + 306, tongueY + 120);
		
		sidebarL.setPosition(0, 0);
		sidebarR.setPosition(800 - sidebarR.getWidth(), 0);

		brain.setX(600);
		brain.setY(328);
		
		stage.addActor(bg);
		
		stage.addActor(tongue);
		stage.addActor(bitter);
		stage.addActor(sweet);
		stage.addActor(sourL);
		stage.addActor(sourR);
		stage.addActor(saltL);
		stage.addActor(saltR);
		
		stage.addActor(sidebarR);
		stage.addActor(sidebarL);
		stage.addActor(brain);		
		for(int i=0;i<4;i++){
			stage.addActor(plate[i]);
			stage.addActor(foods[i]);
		}
		stage.addActor(mulai);
		//stage.addActor(foods[0]);
		
		/* Tutorial Text */
		
		dmanager = new DialogManager(getAtlas().findRegion("dialogbox"), getAtlas().findRegion("pointer_left"),  getAtlas().findRegion("pointer_right"));
		
		if(game.getMode().equals("story")){
			dmanager.addMsg("Oh iya, namaku otak.", 116, 330);
			dmanager.addMsg("Aku akan mengajarimu cara bermain.", 116, 330);
			dmanager.addMsg("Tekan salah satu makanan yang ada\ndikanan...", 170, 94, 580, 221, Direction.RIGHT);
			dmanager.addMsg("Lalu pilihlah area lidah yang\nsesuai dengan warna piringnya.", 180, 244, 181, 139, Direction.RIGHT);
			dmanager.addMsg("Bar ini menunjukkan seberapa jauh\nlagi tujuanmu.", 97, 267, 120, 200, Direction.LEFT);
			dmanager.addMsg("Bar ini menunjukkan seberapa banyak\nwaktu lagi yang tersisa.", 97, 267, 70, 200, Direction.LEFT);
			dmanager.addMsg("Oke, selamat bermain!", 116, 330);
		} else {
			dmanager.addMsg("Tekan salah satu makanan...", 170, 94, 580, 221, Direction.RIGHT);
			dmanager.addMsg("Lalu pilihlah area lidah yang\nsesuai dengan warna piringnya.", 180, 244, 181, 139, Direction.RIGHT);
		}
	
		dmanager.setSkipEnabled(true);
		dmanager.setOnFinishAction(new Runnable(){
			@Override
			public void run() {
				mulai.setVisible(true);
				mulai.addAction(Actions.sequence(Actions.alpha(0f), Actions.fadeIn(0.5f), Actions.delay(0.5f), Actions.run(new Runnable(){
					@Override
					public void run() {
						mulai.remove();
						status = "belom";
					}
				})));
			}
		});
		dmanager.init();
		Runnable showTut = new Runnable(){
			@Override
			public void run() {
				showTutorial();
			}
		};
		
		fadeIn();
		//stage.addAction(Actions.sequence(Actions.delay(1f), Actions.run(showTut)));
		
		
//		Skin skin = new Skin(Gdx.files.internal("skins/menuskin.json"));
//		tutText = new Label("Halo semua, kembali bersama saya, otak!!!", skin);
//		tutText.setFontScale(0.2f);
//		tutText.setPosition(238, 361);
//		stage.addActor(tutText);
		
		loadKepala();
		loadInfus();
		hany.setScaleY(progress/100f);
		poison.setScaleY(time/100f);
		loadHelpPauseButton();
		stage.addActor(dmanager);
		loadPauseScreen();
		
	}
	
	public void startGame(){
		status = "belom";
	}
	
	public void removeForks(){
		for(ClickableFood food: foods){
			food.release();
		}
	}
	
	public String getSelectedFoodColor(){
		return selectedFoodColor;
	}
	
	public void setClickedFood(String data){
		selectedFoodColor = data;
	}
	
	public void showTutorial(){
		//TODO: pause screen ini
		stage.addActor(dmanager);
	}
	
	
	public float getAddedPercent() {
		return addedPercent;
	}

	public void setAddedPercent(float addedPercent) {
		this.addedPercent = addedPercent;
	}

	Runnable r = new Runnable(){

		@Override
		public void run() {
			
		}
		
	};
	
	@Override
	public void render(float delta){
		super.render(delta);
		super.updatePause();
		if (status.equals("menang")) {
			System.gc();
			game.setLevelCompleted(1);
			int score;
			int poisons = (int) (poison.getScaleY()*100f);
			score = poisons * 50;
			game.setArcadeScore(0, score);
			startKepalaAnimation(status, new ScoreScreen(game, "TongueScreen"));
		} else if (status == "kalah") {
			System.gc();
			startKepalaAnimation(status, new GameOverScreen(game));
		}
		if (!getPause() && status == "belom") {		
			//Infus
			if (progress >= 100) {
				progress = 100;
				status = "menang";
			} if (poison.getScaleY() >= 1) {
				status = "kalah";
			}
			if (poison.getScaleY() < 1) {
				time += speed;
				poison.setScaleY((float) (time*0.05/100f));
			}
			
			if(hany.getScaleY() < 1){
				hany.setScaleY(progress/100f);
			}
			//Gdx.app.log(DVenture.LOG, "addedPercent: " + addedPercent);
//			if(progressbar.getHeight() > 0){
//				progressbar.setScale(progressbar.getScaleX(), progressbar.getScaleY() + addedPercent);
//				addedPercent = 0;
//			}
//			
//			if(timebar.getX() > -800)
//				timebar.setX(timebar.getX() - speed*delta);
//			if (timebar.getX() + timebar.getWidth() <= 0) {
//				//game.setScreen(new ModeScreen(game));
//				//game.setScreen(new DuabelasScreen(game));
//			}
	//		Gdx.app.log(DVenture.LOG, "banana coord: " + foods[0].getX() + ", " + foods[0].getY());
	//		Gdx.app.log(DVenture.LOG, "banana image height: " + foods[0].getImageHeight());
	//		Gdx.app.log(DVenture.LOG, "banana image width: " + foods[0].getImageWidth());
	//		Gdx.app.log(DVenture.LOG, "banana z index: " + foods[0].getZIndex());
	//		Gdx.app.log(DVenture.LOG, "banana visibility: " + foods[0].isVisible());	
	//		for(Image food: foods){
	//			if(food.getZIndex() == -1){
	//				stage.addActor(food);
	//			}	
	//		}
			
		}
	}
}
