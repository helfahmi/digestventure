package com.ktat.dventure.main;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Scanner;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.ktat.dventure.screen.SplashScreen;

public class DVenture extends Game {
	public static final String LOG = "DVenture";
	private FPSLogger fpsLogger;
	private TextureAtlas splashAtlas, mainAtlas;
	private AssetManager manager;
	private int lastLevelPlayed;
	private int[] arcadeHighScore;
	private String modeSelect;
	private boolean isMute;
	public static Music menuMusic, storyMusic, arcadeMusic;
	public static Sound buttonSound, soundEffect;
	private Preferences prefs;
	
	@Override
	public void create() {
		Texture.setEnforcePotImages(false);
		fpsLogger = new FPSLogger();
		//mainAtlas = new TextureAtlas(Gdx.files.internal("atlas/DVmain.atlas"));
		splashAtlas = new TextureAtlas(Gdx.files.internal("atlas/DVsplash.atlas"));
		arcadeHighScore = new int[8];
		manager = new AssetManager();
		lastLevelPlayed = -1;
		modeSelect = "story";
		prefs = Gdx.app.getPreferences("User Data Pref");
		loadLevelData();
		setScreen(new SplashScreen(this, manager));
		//setMusic
		menuMusic = Gdx.audio.newMusic(Gdx.files.internal("sound/Pamgaea.mp3"));
		menuMusic.setLooping(true);
		storyMusic = Gdx.audio.newMusic(Gdx.files.internal("sound/StoryMusic.mp3"));
		storyMusic.setLooping(true);
		arcadeMusic = Gdx.audio.newMusic(Gdx.files.internal("sound/ArcadeMusic.mp3"));
		arcadeMusic.setLooping(true);
		buttonSound = Gdx.audio.newSound(Gdx.files.internal("sound/ButtonSound.mp3"));
		soundEffect = Gdx.audio.newSound(Gdx.files.internal("sound/button-3.wav"));
		isMute = false;
	}
	
	public int getLastLevelCompleted(){
		return prefs.getInteger("lastLevel", 0);
	}
	
	public void loadLevelData(){
		lastLevelPlayed = prefs.getInteger("lastLevel", 0);
		for(int i=0;i<8;i++){
			arcadeHighScore[i] = prefs.getInteger("score" + i, 0);
		}
//		FileHandle file = Gdx.files.external("userdata.txt");
//		if(file.exists()){
//			String text = file.readString();
//			Scanner scan = new Scanner(text);
//			String lineString = null;
//			while(scan.hasNext()){
//				lineString = scan.next();
//				if(lineString.charAt(0) == '#')
//					continue;
//			}
//			
//			Gdx.app.log(DVenture.LOG, lineString);
//			lastLevelPlayed = Integer.parseInt(lineString);
//			
//			int index = 0;
//			while(scan.hasNext()){
//				lineString = scan.next();
//				if(lineString.charAt(0) == '#')
//					continue;
//				arcadeHighScore[index++] = Integer.parseInt(lineString);
//				Gdx.app.log(DVenture.LOG, lineString);
//			}
//			scan.close();
//		} else {
//			
//		}
	}
	
	public boolean getLevelCompleted(int regionNumber){
		return (regionNumber <= prefs.getInteger("lastLevel", 0) ? true : false);
	}
	
	public void setLevelScore(int levelNumber, int score){
		prefs.putInteger("score" + levelNumber, score);
		prefs.flush();
	}
	
	public void setLevelCompleted(int regionNumber){
		prefs.putInteger("lastLevel", regionNumber);
		prefs.flush();
//		lastLevelPlayed = regionNumber;
//		FileHandle file = Gdx.files.external("userdata.txt");
//		BufferedWriter wr = new BufferedWriter(file.writer(false));
//		
//		try {
//			wr.append(String.valueOf(regionNumber));
//			wr.newLine();
//			for(int i=0;i<8;i++){
//				wr.append(String.valueOf(arcadeHighScore[i]));
//				wr.newLine();
//			}
//			wr.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
	}
	
	public String getMode(){
		return modeSelect;
	}
	
	public void setMode(String mode){
		modeSelect = mode;
	}
	
	@Override
    public void render(){
        super.render();
        
        if(Gdx.input.isTouched()){
        	//Gdx.app.log("Mouse", "x,y: " + Gdx.input.getX() + " " + (480 - Gdx.input.getY()));
        }
        // output the current FPS
        //fpsLogger.log();
    }
	
	public void setMainAtlas(TextureAtlas atlas){
		mainAtlas = atlas;
	}
	
	public TextureAtlas getMainAtlas(){
		return mainAtlas;
	}
	
	public TextureAtlas getSplashAtlas(){
		return splashAtlas;
	}
	
	public void setMute(boolean mute){
		isMute = mute;
	}
	
	public boolean getMute(){
		return isMute;
	}
	
	public void playMenu(){
		if (!isMute){
			menuMusic.play();
		}
	}
	
	public void pauseMenu(){
		menuMusic.pause();
	}
	
	public void playStory(){
		if (!isMute){
			storyMusic.play();
		}
	}
	
	public void pauseStory(){
		storyMusic.pause();
	}
	
	public void playArcade(){
		if (!isMute){
			arcadeMusic.play();
		}
	}
	
	public void pauseArcade(){
		arcadeMusic.pause();
	}
	
	public void playButtonSound(){
		if (!isMute){
			buttonSound.play(0.2f);
		}
	}
	
	public void playSoundEffect(){
		if (!isMute){
			soundEffect.play(0.2f);
		}
	}
	public int getArcadeScore(int index){
		return arcadeHighScore[index];
	}
	
	public void setArcadeScore(int index, int score){
		arcadeHighScore[index] = score;
	}
	
	@Override
	public void dispose(){
		super.dispose();
		manager.dispose();
		if(splashAtlas != null)
			splashAtlas.dispose();
	}
}
